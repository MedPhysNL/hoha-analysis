"""
This script analyzes the contrast-to-noise ratio (CNR) of the NEMA phantom
and plots the CNR for multiple filters, per imaging site.
The plots are presented as Figure S.13 to S.19.
The script performs the following:
- Loads pre-computed results and configuration data from YAML files.
- Averages the data over the number of acqusitions.
- Plots the data.

Modules and Libraries:
- Core modules: `yaml`, `numpy`, `matplotlib`.
- Custom modules: `register_masks`

Output:
- Plots visualizing the CNRs versus the insert diameters, per reconstruction protocol
"""
# Import necessary libraries
import os  # For file and path operations
import yaml  # For reading configuration and results stored in YAML files
import numpy as np  # For numerical operations and array manipulations
import matplotlib.pyplot as plt  # For generating and displaying plots
import register_masks  # Custom module for sphere diameters

plt.rcParams.update({'font.size': 16})

SITES = [   "LUMC Discovery670",
            "MUMC Discovery870",
            "NKI Intevo",
            "Radboud Intevo",
            "NKI SymbiaT16",
            "Radboud SymbiaT16",
            "UMCU SymbiaT16",   
            ]

LEGEND = ["GE 670",
        "GE 870",
        "Siemens Intevo 1",
        "Siemens Intevo 2",
        "Siemens Symbia 1",
        "Siemens Symbia 2",
        "Siemens Symbia 3",
        ]

markers_plot = ['o','v','^','<','>','s','P']

line_filters = ["-","--","-.",":"]

# Labels for reconstruction protocols and their corresponding plot labels
LABELS = ["AcqA_DEW", "AcqA_TEW", "AcqA_HER", "AcqB", "AcqB_HER"]
LABELS_PLOT = ["DEW 15%", "TEW 15%", "MC 15%", "TEW 20%", "MC 20%"]


# Select which filtered data is to be plotted (0 filter means no filtering)
# FILTER_SIZE_MM = [0, 5, 7.5, 10, 15]
FILTER_SIZE_MM = [0, 5, 10, 15]

# Define the x-axis position the data for the lung insert will be plotted at
lungDiameter_mm = 50

# Define the x-ticks
x_ticks = np.append(np.array(register_masks.SPHERES_MM), lungDiameter_mm)

# Set how many measurements to expect (for initializing data arrays)
numRepeatedMeas = 3

# Define colors for plotting
colors = ['tab:red', 'tab:green', 'tab:blue', 'tab:orange', 'tab:purple', 'tab:cyan', 'tab:olive', 'tab:gray', 'tab:brown', 'tab:pink']

# Load results and configuration data from YAML files
with open("results_NEMA.yml", "r") as f:
    results = yaml.safe_load(f)
with open("config_data.yml", "r") as f:
    conf = yaml.safe_load(f)

# Helper function to load results for a specific site and reconstruction method
def load_results(site, filter_size, label):
    all_CRCs = []
    all_lCRC = []
    all_CNRs = []
    all_lCNR = []
    all_COV = []
    
    for i in [1,2,3]:
        try:
            res = results[site][filter_size][label][i]
        except KeyError:
            print("NOT DEFINED:", site, "NEMA", str(i)+"_"+label)
            continue
        
        fill_ratio = conf["Site data"][site]["NEMA"]["Fill_Ratio"]
        sphere_means = res["sphere_means"]
        bgr_mean = res["bgr_mean"]
        bgr_std = res["bgr_std"]
        lung_mean = res["lung_mean"]
        
        CRCs = [(sph_mean/bgr_mean-1)/(fill_ratio-1) for sph_mean in sphere_means]            
        lCRC = 1-lung_mean/bgr_mean
        COV = bgr_std/bgr_mean
        
        CNRs = [(sph_mean-bgr_mean)/bgr_std for sph_mean in sphere_means]  
        lCNR = (lung_mean-bgr_mean)/bgr_std
        
        all_CRCs.append(CRCs)
        all_lCRC.append(lCRC)
        all_CNRs.append(CNRs)
        all_lCNR.append(lCNR)
        all_COV.append(COV)
        
    
    if not len(all_CRCs):
        return None, None, None, None, None, None, None, None, None, None
    
    av_CRCs = np.array(all_CRCs).mean(axis=0)
    av_lCRC = np.mean(all_lCRC)
    av_CNRs = np.array(all_CNRs).mean(axis=0)
    av_lCNR = np.mean(all_lCNR)
    av_COV = np.mean(all_COV)
    
    return av_CRCs, av_lCRC, av_CNRs, av_lCNR, av_COV, np.array(all_CRCs), np.array(all_lCRC), np.array(all_CNRs), np.array(all_lCNR), np.array(all_COV)

# Conversion factor for mm to inches (for plotting)
mm_to_inch = 0.0393701

# Loop over SITES, create one plot per SITES
for isite, site in enumerate(SITES):
    fig_CNR, ax_CNR = plt.subplots(2, 3, figsize = (500*mm_to_inch, 400*mm_to_inch), sharey = True)#, dpi=600)

    # make arrays that holds all the mean CNR information
    all_av_CNRs = np.zeros([len(LABELS)*len(SITES),6]) 
    all_av_CNRs[:] = np.nan
    all_av_lCNR = np.zeros([len(LABELS)*len(SITES),1]) 
    all_av_lCNR[:] = np.nan
    
    # Initialize variables to arrange which subplot is plotted in
    num_row = 0
    num_col = 0
    for ilab, label in enumerate(LABELS):
        print(label)
        # make arrays that holds all the CNR information, is regenerated for each new label
        all_meas_CNRs = np.zeros([len(SITES)*numRepeatedMeas,6]) 
        all_meas_CNRs[:] = np.nan
        all_meas_lCNR = np.zeros([len(SITES)*numRepeatedMeas,1]) 
        all_meas_lCNR[:] = np.nan

        # Loop over the filters
        for ifilter, filter_size in enumerate(FILTER_SIZE_MM):
            av_CRCs, av_lCRC, av_CNRs, av_lCNR, av_COV, all_CRCs, all_lCRC, all_CNRs, all_lCNR, all_COV  = load_results(site, filter_size, label)       
            
            
            if not (av_CNRs is None):    
                index_av = (ilab*len(SITES))+isite
                index = (isite*(numRepeatedMeas-1))+isite

                all_av_CNRs[index_av,:] = av_CNRs
                all_av_lCNR[index_av,:] = av_lCNR    

                all_meas_CNRs[index:(index+all_CNRs.shape[0]),:] = all_CNRs
                all_meas_lCNR[index:(index+all_lCNR.shape[0]),:] = all_lCNR.reshape([all_lCNR.shape[0],1])            
                
                # plot the lung marker on togther with the sphere markers, otherwise there is no way of telling which filter size it represents
                y_data = np.append(av_CNRs, av_lCNR)
                ax_CNR[num_row,num_col].plot(x_ticks, y_data, markers_plot[isite], linestyle=line_filters[ifilter], label=('Filter '+str(filter_size)+' mm FWHM'), color = colors[isite]) 
                
            else: # plot made-up data outside of the visible graph to make sure that all SITES are displayed in the legend
                ax_CNR[num_row,num_col].plot(register_masks.SPHERES_MM, [-1000,-1000,-1000,-1000,-1000,-1000], 'o-', label=LEGEND[isite], color = colors[isite])

            if (ilab == 0) and (ifilter == 0):
                ax_CNR[num_row,num_col].set_ylabel("Contrast-to-noise ratio", weight='bold', y=-0.1)
                
            if site == "LUMC Discovery670" and (ilab == 0) and (ifilter == 0):
                ax_CNR[num_row,num_col].set_ylabel("Contrast-to-noise ratio", weight='bold')
                
            if site == "LUMC Discovery670" and (ilab == 1) and (ifilter == 0):
                ax_CNR[num_row,num_col].set_xlabel("Insert diameter [mm]", weight='bold')

            if (num_row == 1) and (num_col == 1) and (ifilter == 0):
                ax_CNR[num_row,num_col].set_xlabel("Insert diameter [mm]", weight='bold')
                

        ax_CNR[num_row,num_col].set_xlim(left=0)
        ax_CNR[num_row,num_col].set_ylim(bottom=-7, top=28)
        ax_CNR[num_row,num_col].grid()
        ax_CNR[num_row,num_col].axvline(43.5, linestyle='--', color='black')
        
        label = LABELS_PLOT[ilab]
        if site == "LUMC Discovery670": 
            if num_row == 0:
                ax_CNR[num_row,num_col].set_title(label, weight='bold')
        else: 
            ax_CNR[num_row,num_col].set_title(label, weight='bold')
        
        # make sure to plot in the correct subplot:
        if ilab == 2:
            num_row += 1
        num_col += 1
        if num_row == 1 and num_col==3:
            num_col = 1
        
    ax_CNR[1,0].axis('off') 
    
    ax_CNR[0,0].legend(loc='upper left')  
    
    if site == "LUMC Discovery670":
        ax_CNR[1, 1].axis('off')
        ax_CNR[1, 2].axis('off')
    else:
        ax_CNR[1, 1].tick_params(labelleft=True)
          
    # Set the figure title
    fig_CNR.suptitle(LEGEND[isite], weight='bold', y=0.93)
    
    # Set the figure name
    fn_figure_CNR = 'NEMA_filtered_CNRs_' + SITES[isite] + '.png'
    fig_CNR.subplots_adjust(wspace=0.1, hspace=0.15)
    # Save the figure
    fig_CNR.savefig(os.path.join("NEMA_figures_filter_per_center", fn_figure_CNR), dpi=600, orientation='landscape',format='png',  bbox_inches='tight')    
    
    print(" ") 

    plt.draw()
    plt.pause(2)
    plt.close()
