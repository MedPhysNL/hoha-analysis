"""
This script analyzes and visualizes the converge of NEMA data (CRC, CNR and COV for data reconstructed 
with varying number of iterations/updates) from various imaging sites. 
The plot is presented as Figure S.5.

The script performs the following:
- Loads pre-computed results and configuration data from YAML files.

- Saves the plot.

Modules and libraries used:
- Custom modules: `register_masks`

Output:
- A plot visualizing the CRC, CNR, and COV as a function of number of iterations in the reconstruction.
"""

# Import necessary libraries and modules
import os  # For file path operations
import yaml  # For reading and writing YAML configuration and results files

import register_masks # Functions for registering and processing masks
import matplotlib.pyplot as plt # For generating and displaying plots


# Define the imaging sites for analysis
SITES = [   "MUMC Discovery870",
             "Radboud Intevo", 
            ]
# Labels for reconstruction protocols
LABELS = ["AcqA_TEW_1i8s", "AcqA_TEW_2i8s", "AcqA_TEW_3i8s", "AcqA_TEW_4i8s", "AcqA_TEW_5i8s", "AcqA_TEW_6i8s", "AcqA_TEW_7i8s", "AcqA_TEW_8i8s", "AcqA_TEW_9i8s", "AcqA_TEW_10i8s", "AcqA_TEW_11i8s", "AcqA_TEW_12i8s", "AcqA_TEW_13i8s", "AcqA_TEW_14i8s", "AcqA_TEW_15i8s"]

# Select which filtered data is to be plotted (0 filter means no filtering)
# FILTER_SIZE_MM = [0, 5, 7.5, 10, 15]
FILTER_SIZE_MM = [0]

# Define colors and markers for plotting
colors = ['tab:red', 'tab:green', 'tab:blue', 'tab:orange', 'tab:purple', 'tab:cyan', 'tab:olive', 'tab:gray', 'tab:brown', 'tab:pink']
markers_plot = ['o','v','^','<','>','s','P']

# find the number of iterations that have been used for reconstucting the dataset, 
# based on the labels of the data (above), this assumes the format of the label 
# to be: "..._1i..." with "1" here being the number of iterations used
# This with be used for the x-ticks in the plot
extractAfter = "TEW_"
extractBefore = "i"
numIterations = []
for ilabel, strlabel in enumerate(LABELS):
    iter = LABELS[ilabel].split(extractAfter)[1].split(extractBefore)[0] 
    numIterations.append(int(iter))

# Set x-lim for the plots
xlimPlts = [1-0.3, numIterations[-1]+0.3]

# Load results and configuration data from YAML files
with open("results_NEMA_convergence.yml", "r") as f:
    results = yaml.safe_load(f)
with open("config_data_NEMA_convergence.yml", "r") as f:
    conf = yaml.safe_load(f)

# Helper function to load results for a specific site and reconstruction method
def load_results(site, filter_size):
    all_CRCs = []
    all_lCRC = []
    all_COV = []
    all_CNRs = []
    all_lCNR = []

    # Define which acquisition is used 
    if site == "MUMC Discovery870":
        acq_list = [1]
    if site == "Radboud Intevo":
        acq_list = [2]
        
    for i in acq_list:
        
        # Loop over each reconstruction protocol (label)
        for ilabel, label in enumerate(LABELS):
            # Load the data
            try:
                res = results[site][filter_size][label][i]
            except KeyError:
                print("NOT DEFINED:", site, "NEMA", "_"+label)
                continue
            
            fill_ratio = conf["Site data"][site]["NEMA"]["Fill_Ratio"]
            sphere_means = res["sphere_means"]
            bgr_mean = res["bgr_mean"]
            bgr_std = res["bgr_std"]
            lung_mean = res["lung_mean"]
            
            CRCs = [(sph_mean/bgr_mean-1)/(fill_ratio-1) for sph_mean in sphere_means]            
            lCRC = 1-lung_mean/bgr_mean
            COV = bgr_std/bgr_mean
            CNRs = [(sph_mean-bgr_mean)/bgr_std for sph_mean in sphere_means]  
            lCNR = (lung_mean-bgr_mean)/bgr_std
            
            all_CRCs.append(CRCs)
            all_lCRC.append(lCRC)
            all_COV.append(COV)
            all_CNRs.append(CNRs)
            all_lCNR.append(lCNR)
        
    return all_CRCs, all_lCRC, all_COV, all_CNRs, all_lCNR


# Conversion factor for mm to inches (for plotting)
mm_to_inch = 0.0393701

# Loop over FILTER_SIZE_MM, create one plot per filter size
for ifilter, filter_size in enumerate(FILTER_SIZE_MM):
    fig, ax = plt.subplots(3, 2, figsize = (280*mm_to_inch, 380*mm_to_inch), sharex = True)
    
    # Define which column to plot in
    num_col = 0
    
    # Loop over each site
    for isite, site in enumerate(sorted(SITES)):
        print("Plot data for center: " + site)
        # Extract the data 
        all_CRCs, all_lCRC, all_COV, all_CNRs, all_lCNR = load_results(site, filter_size)  
        
        if site == "MUMC Discovery870":
            numIterationsPlot = numIterations[0:10]
        else:
            numIterationsPlot = numIterations
            
        print(num_col)
        
        # Plot the data
        ax[0,num_col].plot(numIterationsPlot, all_lCRC, label="Lung insert", marker=markers_plot[0], color = colors[0]) 
        ax[1,num_col].plot(numIterationsPlot, all_lCNR, label="Lung insert", marker=markers_plot[0], color = colors[0]) 
        
        for isphere, sphereDiameter in enumerate(register_masks.SPHERES_MM):
            print("plot sphere")
            ax[0,num_col].plot(numIterationsPlot, [i[isphere] for i in all_CRCs], marker=markers_plot[isphere+1], label="Sphere Ø"+str(sphereDiameter)+" mm", color = colors[isphere+1])
            ax[1,num_col].plot(numIterationsPlot, [i[isphere] for i in all_CNRs], marker=markers_plot[isphere+1], label="Sphere Ø"+str(sphereDiameter)+" mm", color = colors[isphere+1])

        
        ax[2,num_col].plot(numIterationsPlot, all_COV, label="Background NEMA", marker='p', color='gray') 
        
        ax[0,num_col].set_ylim(bottom=-0.1, top=0.8)
        ax[1,num_col].set_ylim(bottom=-5, top=22)
        ax[2,num_col].set_ylim(bottom=0, top=0.8)
        
        ax[0,0].set_title("GE 870", weight='bold')
        ax[0,1].set_title("Siemens Intevo 2", weight='bold')
        
        ax[1,0].legend(loc='upper right') 
        ax[2,0].legend(loc='lower right') 

        
        ax[0,num_col].grid()
        ax[1,num_col].grid()
        ax[2,num_col].grid()
        
        ax[0,0].set_ylabel("Contrast recovery coefficient", weight='bold')
        ax[1,0].set_ylabel("Contrast-to-noise ratio", weight='bold')
        ax[2,0].set_ylabel("Coefficient of variation", weight='bold')
        ax[2,0].set_xlabel("Number of iterations", weight='bold')
        ax[2,1].set_xlabel("Number of iterations", weight='bold')
        
        ax[0, 1].tick_params(labelleft=False)
        ax[1, 1].tick_params(labelleft=False)
        ax[2, 1].tick_params(labelleft=False)
        
        # Mark chosen reconstruction parameters for GE 
        ax[0,0].fill_between([3.5, 3.5, 4.5, 4.5],[0, 22, 22, 0], color='gray', alpha = 0.2)
        ax[0,0].fill_between([3.5, 3.5, 4.5, 4.5],[0, -5, -5, 0], color='gray', alpha = 0.2)
        ax[1,0].fill_between([3.5, 3.5, 4.5, 4.5],[0, 22, 22, 0], color='gray', alpha = 0.2)
        ax[1,0].fill_between([3.5, 3.5, 4.5, 4.5],[0, -5, -5, 0], color='gray', alpha = 0.2)
        ax[2,0].fill_between([3.5, 3.5, 4.5, 4.5],[0, 22, 22, 0], color='gray', alpha = 0.2)
        ax[2,0].fill_between([3.5, 3.5, 4.5, 4.5],[0, -5, -5, 0], color='gray', alpha = 0.2)
        
        # Mark chosen reconstruction parameters for Siemens
        ax[0,1].fill_between([9.5, 9.5, 10.5, 10.5],[0, 22, 22, 0], color='gray', alpha = 0.2)
        ax[0,1].fill_between([9.5, 9.5, 10.5, 10.5],[0, -5, -5, 0], color='gray', alpha = 0.2)
        ax[1,1].fill_between([9.5, 9.5, 10.5, 10.5],[0, 22, 22, 0], color='gray', alpha = 0.2)
        ax[1,1].fill_between([9.5, 9.5, 10.5, 10.5],[0, -5, -5, 0], color='gray', alpha = 0.2)
        ax[2,1].fill_between([9.5, 9.5, 10.5, 10.5],[0, 22, 22, 0], color='gray', alpha = 0.2)
        ax[2,1].fill_between([9.5, 9.5, 10.5, 10.5],[0, -5, -5, 0], color='gray', alpha = 0.2)
               
        num_col += 1
        
          
    # Set figure name
    fn_figure = 'NEMA_convergence.png'
    plt.subplots_adjust(wspace=0.1, hspace=0.10)
    # Save figure
    plt.savefig(os.path.join("NEMA_igures", fn_figure), dpi=600, orientation='landscape',format='png',  bbox_inches='tight')
    plt.draw()
    # plt.pause(2)
    # plt.close()
    
    
