"""
This script analyzes and visualizes the axial uniformity of cylindrical phantom data obtained from CT and SPECT scans.
The plot is presented as Figure 4.
The script performs the following:
- Loads pre-computed results and configuration data from YAML files.
- Fits axial profiles using quadratic functions for uniformity assessment.
- Calculates and visualizes normalized axial homogeneity parameters for various scanners and reconstruction methods.
- Saves visualizations of axial profiles for further analysis.

Modules and Libraries:
- Core modules: `os`, `yaml`, `numpy`, `matplotlib`, `scipy`.
- Custom visualization and processing functions for profile fitting and plotting.

Output:
- A plot visualizing the axial uniformity for each scanner and reconstruction method.
- Mean and standard deviation of the fitted uniformity parameter are printed to the console.
"""
# Import necessary libraries
import os  # For file and path operations
import yaml  # For reading configuration and results stored in YAML files
import numpy as np  # For numerical operations and array manipulations
import matplotlib.pyplot as plt  # For generating and displaying plots
import scipy
from scipy.optimize import curve_fit  # For fitting functions to data

# Define the imaging sites analyzed
SITES = [   "LUMC Discovery670",
            "MUMC Discovery870",
            "NKI Intevo",
            "Radboud Intevo",
            "NKI SymbiaT16",
            "Radboud SymbiaT16",
            "UMCU SymbiaT16",   
            ]

# Corresponding scanner IDs
scannerID = ["GE 670",
            "GE 870",
            "Siemens Intevo 1",
            "Siemens Intevo 2",
            "Siemens Symbia 1",
            "Siemens Symbia 2",
            "Siemens Symbia 3",
            ]

# Labels for reconstruction protocols and their corresponding plot labels
LABELS = ["AcqA_DEW", "AcqA_TEW", "AcqA_HER"] # Acquisition A protocols (15% main photopeak)
LABELS_PLOT = ["DEW", "TEW", "MC"] # Shorter names for plots
# Define colors for plotting
colors = ['tab:red', 'tab:green', 'tab:blue', 'tab:orange', 'tab:purple', 'tab:cyan', 'tab:olive', 'tab:gray', 'tab:brown', 'tab:pink']

# Load results and configuration data from YAML files
with open("results_cylinder_uniformity.yml", "r") as f:
    results = yaml.safe_load(f)
with open("config_data.yml", "r") as f:
    conf = yaml.safe_load(f)
    
# Helper function to load results for a specific site and reconstruction method
def load_results(site, label):
    """
    Load the axial profiles, center slices, voxel sizes, and phantom dimensions 
    for a specific site and reconstruction method.
    """
    all_centerSlice = [] # Store center slice positions, axial direction
    all_axial_profile_sum = [] # Store axial profile sums
    all_voxelsize = [] # Store voxel sizes
    
    for i in [1]: # Iterate through acquisition indices
        try:
            res = results[site][label][i] # Load results for the site and label
        except KeyError:
            print("NOT DEFINED:", site, "Cylinder", str(i)+"_"+label)
            continue
        
        # Extract phantom dimensions and data
        phantom_diameter = conf["Site data"][site]["Cylinder"]["Phantom_dia_mm"]
        phantom_length = conf["Site data"][site]["Cylinder"]["Phantom_len_mm"]
        centerSlice = res["Center slice axial direction"]      
        axial_profile_sum = res["Sum of counts in slice"]
        voxelsize = res["Voxel size SPECT"]

        # Append the results to corresponding lists
        all_centerSlice.append(centerSlice)
        all_axial_profile_sum.append(axial_profile_sum)
        all_voxelsize.append(voxelsize)

    # If no data is available, return None
    if not len(all_axial_profile_sum):
        return None, None, None, None, None

    # Return the loaded data
    return all_axial_profile_sum, all_centerSlice, all_voxelsize, phantom_diameter, phantom_length

# Initialize arrays to store fit results
fit_axial = np.zeros((len(SITES), len(LABELS)))
fit_axial_normalized = np.zeros((len(SITES), len(LABELS)))

# Conversion factor for mm to inches (for plotting)
mm_to_inch = 0.0393701

# Create subplots for visualizing axial uniformity
fig_axial_fit, ax_axial_fit = plt.subplots(len(SITES), len(LABELS), figsize = (180*mm_to_inch, 220*mm_to_inch), sharex = True, sharey = True)

# Loop over each reconstruction protocol (label)
for ilab, label in enumerate(LABELS):
    print("Reconstruction: " + label)
    
    # Loop over each site
    for isite, site in enumerate(SITES):
        print("Scanner: " + site)
        # Load results for the current site and reconstruction method
        all_axial_profile_sum, all_centerSlice, all_voxelsize, phantom_diameter, phantom_length = load_results(site, label)    
        
        if all_axial_profile_sum is None:
            fit_axial_normalized[isite,ilab] = np.nan # If no data, set to NaN
        else:
            # Extract and trim the axial profile
            profile = np.array(all_axial_profile_sum[0])
            start = np.where(profile>0)[0][0]
            end = np.where(profile>0)[0][-1]
            profile = profile[start:end+1]  # Keep only non-zero parts
            
            # Generate the x-axis coordinates (in cm)
            x_axis_coords = np.arange(len(profile))*all_voxelsize[0][0]/10
            
            # Define a function for fitting the profile

            def fitfunc(x, center, width, res, scale, offset, curvature):
                left = center - width/2
                right = center + width/2
                block = scipy.special.expit((x-left)*res) * scipy.special.expit(-(x-right)*res) #Sigmoid block function
                quadr = curvature * ((x-center)/width)**2 + 1 # Quadratic curvature
                return scale * (block * quadr * (1-offset) + offset) # Offset and scaling
                        
            # Perform the fitting
            ctr_guess = x_axis_coords[len(profile)//2] # Initial guess for the center
            height_guess = 0.95*profile.max() # Initial guess for the height
            p0 = (ctr_guess, phantom_length/10, 0.25, height_guess, profile[0]/height_guess, 0)            
            popt, _ = curve_fit(fitfunc, x_axis_coords, profile, p0=p0)

            # Store the normalized curvature parameter
            fit_axial_normalized[isite,ilab] = popt[-1]
            
            # Normalize and center the profile
            profile /= popt[3]; popt[3] = 1
            x_axis_coords -= popt[0]; popt[0] = 0
            
            # Plot the profile and the fitted curve         
            ax_axial_fit[isite,ilab].plot(x_axis_coords, profile,'-', label=site, color=colors[isite], linewidth=2)
            ax_axial_fit[isite,ilab].set_xlim(left=-20, right=20)
            ax_axial_fit[isite,ilab].set_ylim(bottom=0, top=1.2)
                        
            # Plot extended fitted curvature
            xcoords = np.arange(-20, 20, 0.1)
            extended_curvature = popt[3] * (popt[5] * (xcoords/popt[1])**2 + 1) * (1-popt[4]) + popt[4]
            ax_axial_fit[isite,ilab].plot(xcoords, extended_curvature,':', label=site, color='k', linewidth=1)
            
        # Add text showing the fit parameter
        fit_parameter = "%.2f"%(fit_axial_normalized[isite,ilab])
        ax_axial_fit[isite,ilab].text(0,0.55,fit_parameter, fontsize = 8, horizontalalignment='center')        
           
        # Add labels and titles to the subplots                     
        if (isite == 0):                                                                         
            ax_axial_fit[isite,ilab].set_title(LABELS_PLOT[ilab], weight='bold')
        
        if (ilab == 0):
            ax_axial_fit[isite,ilab].set_ylabel(scannerID[isite], rotation=80)            
            
        if (ilab == 1) and (isite == len(SITES)-1):
            ax_axial_fit[isite,ilab].set_xlabel("Axial distance from center [cm]", weight='bold')
            
    print()
# Print mean and standard deviation of the normalized fit parameter for each reconstruction method
print("Mean", np.mean(fit_axial_normalized,0) )
print("Std",  np.std(fit_axial_normalized,0) )

# Set the figure name
fn_figure_axial_fit = "uniformity_cylindrical_phantom_15%photopeak.png"

fig_axial_fit.tight_layout()

# Save the figure
fig_axial_fit.savefig(os.path.join("Cylinder_figures", fn_figure_axial_fit), dpi=600, orientation='portrait',format='png',  bbox_inches='tight')

plt.show()
