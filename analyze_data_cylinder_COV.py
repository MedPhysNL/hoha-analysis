"""
This script calculates the coefficient of variation (COV) based on the SPECT data masked with a CT-based mask.
This script generates the data presented in Table 4 (`COV_all_A`) and Table S.2 (`COV_all_B`).
The process includes:
- Loading CT and SPECT data for various imaging sites.
- Transforming and aligning CT data to match SPECT coordinates.
- Creating and modifying masks for the cylindrical phantom. The CT-based mask is 
eroded based on `margin_from_edge_mm`.
- Calculating the COV per slice in the masked SPECT volume, and the mean COV over all slices.
- Saving the results in a YAML file.

Modules and libraries used:
- Custom modules: `register_masks`, `transform_lib`, `cylinder_functions`.
- Libraries: `numpy`, `scipy`, `skimage`, `yaml`, `matplotlib`.

Output:
- Detailed results are stored in `results_cylinder_COV.yml`.
- Warnings and intermediate values are printed to the console.
"""

# Import necessary libraries and modules
import os  # For file path operations
import yaml  # For reading and writing YAML configuration and results files
import register_masks  # Custom module for applying masks to the data
import transform_lib  # Custom module for transformations and data loading
import cylinder_functions  # Custom module for cylinder-related operations
import numpy as np  # For numerical operations
from scipy.ndimage import center_of_mass, morphology  # For image processing
import skimage.morphology as morph  # For morphological operations on images

# Define the imaging sites for analysis
SITES = [   "LUMC Discovery670",
            "MUMC Discovery870",
            "NKI Intevo",
            "Radboud Intevo",
            "NKI SymbiaT16",
            "Radboud SymbiaT16",
            "UMCU SymbiaT16",   
            ]

# Labels for acquisition / reconstruction protocols
LABELS = {
    "A": ["AcqA_DEW", "AcqA_TEW", "AcqA_HER"], # Acquisition A protocols (15% main photopeak)
    "B": ["AcqB", "AcqB_HER"] # Acquisition B protocols (20% main photopeak)
}

# File paths for storing results and reading configurations
RESULT_YAML = "results_cylinder_COV.yml"  # Path to the results file
CONFIG_YAML = "config_data.yml"  # Path to the configuration file

# Threshold values for defining CT mask in Hounsfield Units (HU), defined for each SITES
threshold_HU = [-80,-80,-130,-130,-130,-130,-130];

# Parameters for image processing and masking
CT_filter_size = 15
margin_from_edge_mm = 30

if __name__ == "__main__":
    COV_all_A = np.zeros([len(SITES),len(LABELS['A'])]) # Array for COV of Acq A
    COV_all_B = np.zeros([len(SITES),len(LABELS['B'])]) # Array for COV of Acq B
    
    # Loop through each imaging site
    for isite,SITE in enumerate(SITES):
        print("Scanner analyzed:", SITE)
        
        # Load existing results if available, otherwise initialize an empty dictionary
        try:
            with open(RESULT_YAML, "r") as f:
                results = yaml.safe_load(f)
        except FileNotFoundError:
            results = {}
        
        # Load the configuration data
        with open(CONFIG_YAML, "r") as f:
            conf = yaml.safe_load(f)
        
        # List of acquisition indices to analyze
        acq_list = [1]
        
        # Loop through each acquisition and protocol
        for i in acq_list:
            for acq in ["A", "B"]:
                # Retrieve the path to the CT data for the current acquisition and site
                try:
                    path_CT = os.path.join(conf["Data folder"], conf["Site data"][SITE]["Cylinder"][str(i)+"_CT"+acq])
                    print("CT data loaded: " + path_CT)
                except KeyError:
                    print("NOT DEFINED:", SITE, "Cylinder", str(i)+"_CT"+acq)
                    continue
            
                # Default identity matrix for aligning SPECT and CT data
                transSPECTtoCT = np.array([[1, 0, 0, 0],
                                           [0, 1, 0, 0],
                                           [0, 0, 1, 0],
                                           [0, 0, 0, 1]])
                
                # Load CT data and associated metadata
                data_CT, voxelsize_CT, imagePosition_CT, imageOrientation_CT, headers_CT = transform_lib.loadData( path_CT, return_headers=True)
                
                # Generate affine transformation matrices for CT data
                A_CT, iA_CT = transform_lib.genDICOMAffine3D(imagePosition_CT, imageOrientation_CT, voxelsize_CT)

                # Retrieve the frame of reference UID for CT data
                FoR_CT = headers_CT[0].FrameOfReferenceUID
                     
                # Retrieve the phantom's dimensions from the configuration file
                phantom_diameter = conf["Site data"][SITE]["Cylinder"]["Phantom_dia_mm"]
                phantom_length = conf["Site data"][SITE]["Cylinder"]["Phantom_len_mm"]
                
                # Calculate and print the phantom volume (cylinder)
                phantom_volume = np.pi * (phantom_length / 10) * \
                                 ((phantom_diameter / 10) / 2) ** 2
                print("Phantom volume (ml):", str(phantom_volume))

                # Process data for each protocol label
                for ilab, label in enumerate(LABELS[acq]):
                    # Retrieve the path to the SPECT data
                    try:
                        path_SPECT = os.path.join(conf["Data folder"], conf["Site data"][SITE]["Cylinder"][str(i)+"_"+label]) #refers to config file
                    except KeyError:
                        print("NOT DEFINED:", SITE, "Cylinder", str(i)+"_"+label)
                        continue
                    
                    print(i, label)
                    
                    # Load SPECT data and associated metadata
                    data_SPECT, voxelsize_SPECT, imagePosition_SPECT, imageOrientation_SPECT, headers_SPECT = transform_lib.loadData( path_SPECT, return_headers=True )
                    
                    # Generate affine transformation matrices for SPECT data
                    A_SPECT, iA_SPECT = transform_lib.genDICOMAffine3D(imagePosition_SPECT, imageOrientation_SPECT, voxelsize_SPECT)                
                    
                    # Retrieve the frame of reference UID for SPECT data
                    FoR_SPECT = headers_SPECT.FrameOfReferenceUID
                    
                    # Warn if frame of reference UIDs do not match between CT and SPECT
                    if FoR_CT != FoR_SPECT:
                        print("WARNING: Frame of reference UID mismatch between CT and SPECT.")
                        print("CT path:", path_CT)
                        print("SPECT path:", path_SPECT)
                                            
                    # Transform CT data to align with SPECT data
                    data_CT_transformed = cylinder_functions.transform_CT_to_SPECT(A_SPECT, data_SPECT, iA_CT, data_CT, transSPECTtoCT)

                    # Correct CT data values outside the reconstruction region (needed for GE data)
                    data_CT_transformed[data_CT_transformed < -1024] = -1024
                
                    # Filter CT data for smoothing
                    data_CT_transformed_fltr = register_masks.filter(data_CT_transformed, voxelsize_SPECT, CT_filter_size)
                                        
                    # Create a binary mask for the phantom
                    mask_CT_transformed = data_CT_transformed_fltr > threshold_HU[isite]
                    
                    # Calculate and print the mask volume
                    mask_volume = np.prod(voxelsize_SPECT / 10) * np.sum(mask_CT_transformed)
                    print("CT mask volume (ml):", str(mask_volume))

                    # Calculate the center of mass for the mask
                    center_CT_transformed = center_of_mass(mask_CT_transformed)
                    coord_center_CT_transformed = [int(round(coord)) for coord in center_CT_transformed]
                    
                    # Plot the CT and mask for the first protocol
                    if ilab == 0:
                        cylinder_functions.plot_CT_cylinder_mask(data_CT_transformed, mask_CT_transformed, data_SPECT, coord_center_CT_transformed, np.abs(voxelsize_SPECT), SITE + label)
                    
                    # Create eroded mask
                    ball = morph.ball(np.abs(round(margin_from_edge_mm/voxelsize_SPECT[0])))
                    mask_CT_transformed_erode = np.zeros_like(data_SPECT)
                    mask_CT_transformed_erode = morphology.binary_erosion(mask_CT_transformed, structure=ball)
                    
                    # Plot the CT and mask for the first protocol and print volume
                    if ilab == 0:
                        cylinder_functions.plot_CT_cylinder_mask(data_CT_transformed, mask_CT_transformed_erode, data_SPECT, coord_center_CT_transformed, np.abs(voxelsize_SPECT), SITE+label)
                    print("CT mask volume erode (ml): ", str(np.prod(voxelsize_SPECT/10)*np.sum(mask_CT_transformed_erode)))
                                        
                    masked_data_SPECT = data_SPECT[mask_CT_transformed_erode]
                    
                    # Initialize variables and fill them in
                    meanPerSlice = np.empty(0)
                    stdPerSlice = np.empty(0)
                    COVperSlice = np.empty(0)
                    COVmean = np.empty(0)
                    for iSlice in range(data_SPECT.shape[0]):
                        temp_slice = data_SPECT[iSlice,:,:];
                        # if the mask is present in the slice, add the mean and std values
                        if (np.sum(mask_CT_transformed_erode[iSlice,:,:]) > 0):
                            meanPerSlice = np.append(meanPerSlice, np.mean(temp_slice[mask_CT_transformed_erode[iSlice,:,:]]))
                            stdPerSlice = np.append(stdPerSlice, np.std(temp_slice[mask_CT_transformed_erode[iSlice,:,:]]))
                    
                    # Calculate the COV
                    COVperSlice = stdPerSlice/meanPerSlice
                    COVmean = np.mean(COVperSlice)
                                        
                    if acq == "A":
                        COV_all_A[isite,ilab] = COVmean
                    if acq == "B":
                        COV_all_B[isite,ilab] = COVmean
 
                    # Save the results in the YAML file
                    res = results.setdefault(SITE, {})
                    res = res.setdefault(margin_from_edge_mm, {})
                    res = res.setdefault(label, {})
                    res[i] = {
                        "COVperSlice": [float(x) for x in COVperSlice],
                        "COVmean": [float(COVmean)],
                    }
                    print(" ")
                    with open(RESULT_YAML, "w") as f:
                        yaml.dump(results, f, default_flow_style = None)
                        
      