"""
This script evaluates the uniformity of cylindrical phantom data from CT and SPECT scans. 
The script generates data used for Figure 4 and Figure S.2
The process includes:
- Loading CT and SPECT data for various imaging sites.
- Transforming and aligning CT data to match SPECT coordinates.
- Creating and modifying masks for the cylindrical phantom.
- Calculating metrics like mask volume, center of mass, and axial profiles.
- Saving the results in a YAML file for further analysis.

Modules and libraries used:
- Custom modules: `register_masks`, `transform_lib`, `cylinder_functions`.
- Libraries: `numpy`, `scipy`, `skimage`, `yaml`, `matplotlib`.

Output:
- Detailed results are stored in `results_cylinder_uniformity.yml`.
- Warnings and intermediate values are printed to the console.
"""

# Import necessary libraries and modules
import os  # For file path operations
import yaml  # For reading and writing YAML configuration and results files
import register_masks  # Custom module for applying masks to the data
import transform_lib  # Custom module for transformations and data loading
import cylinder_functions  # Custom module for cylinder-related operations
import numpy as np  # For numerical operations
from scipy.ndimage import center_of_mass, morphology  # For image processing
import matplotlib.pyplot as plt  # For plotting and visualization
import skimage.morphology as morph  # For morphological operations on images
from skimage.filters import threshold_otsu  # For automatic thresholding

# Define the imaging sites for analysis
SITES = [   "LUMC Discovery670",
            "MUMC Discovery870",
            "NKI Intevo",
            "Radboud Intevo",
            "NKI SymbiaT16",
            "Radboud SymbiaT16",
            "UMCU SymbiaT16",   
            ]

# Labels for acquisition / reconstruction protocols
LABELS = {
    "A": ["AcqA_DEW", "AcqA_TEW", "AcqA_HER"], # Acquisition A protocols (15% main photopeak)
    "B": ["AcqB", "AcqB_HER"] # Acquisition B protocols (20% main photopeak)
}



# File paths for storing results and reading configurations
RESULT_YAML = "results_cylinder_uniformity.yml"  # Path to the results file
CONFIG_YAML = "config_data.yml"  # Path to the configuration file

# Threshold values for defining CT mask in Hounsfield Units (HU), defined for each SITES
threshold_HU = [-80, -80, -130, -130, -130, -130, -130]

# Parameters for filtering and visualization
CT_filter_size = 15  # Smoothing filter size for CT images
colormap_CT = 'gray'  # Colormap for displaying CT images
colormap_SPECT = 'hot'  # Colormap for displaying SPECT images
interp = 'none'  # Interpolation method for image visualization

# Main block to execute the script
if __name__ == "__main__":
    # Loop through each imaging site
    for isite,SITE in enumerate(SITES):
        print("Scanner analyzed:", SITE)
        
        # Load existing results if available, otherwise initialize an empty dictionary
        try:
            with open(RESULT_YAML, "r") as f:
                results = yaml.safe_load(f)
        except FileNotFoundError:
            results = {}
        
        # Load the configuration data
        with open(CONFIG_YAML, "r") as f:
            conf = yaml.safe_load(f)
        
        # List of acquisition indices to analyze
        acq_list = [1]

        # Loop through each acquisition and protocol
        for i in acq_list:
            for acq in ["A", "B"]:
                # Retrieve the path to the CT data for the current acquisition and site
                try:
                    path_CT = os.path.join(conf["Data folder"], conf["Site data"][SITE]["Cylinder"][str(i)+"_CT"+acq])
                    print("CT data loaded: " + path_CT)
                except KeyError:
                    print("NOT DEFINED:", SITE, "Cylinder", str(i)+"_CT"+acq)
                    continue
            
                # Default identity matrix for aligning SPECT and CT data
                transSPECTtoCT = np.array([[1, 0, 0, 0],
                                           [0, 1, 0, 0],
                                           [0, 0, 1, 0],
                                           [0, 0, 0, 1]])

                # Load CT data and associated metadata
                data_CT, voxelsize_CT, imagePosition_CT, imageOrientation_CT, headers_CT = \
                    transform_lib.loadData(path_CT, return_headers=True)

                # Generate affine transformation matrices for CT data
                A_CT, iA_CT = transform_lib.genDICOMAffine3D(
                    imagePosition_CT, imageOrientation_CT, voxelsize_CT
                )

                # Retrieve the frame of reference UID for CT data
                FoR_CT = headers_CT[0].FrameOfReferenceUID

                # Retrieve the phantom's dimensions from the configuration file
                phantom_diameter = conf["Site data"][SITE]["Cylinder"]["Phantom_dia_mm"]
                phantom_length = conf["Site data"][SITE]["Cylinder"]["Phantom_len_mm"]

                # Calculate and print the phantom volume (cylinder)
                phantom_volume = np.pi * (phantom_length / 10) * \
                                 ((phantom_diameter / 10) / 2) ** 2
                print("Phantom volume (ml):", str(phantom_volume))
                
                # Process data for each protocol label
                for ilab, label in enumerate(LABELS[acq]):
                    # Retrieve the path to the SPECT data
                    try:
                        path_SPECT = os.path.join(conf["Data folder"], conf["Site data"][SITE]["Cylinder"][str(i)+"_"+label]) #refers to config file
                    except KeyError:
                        print("NOT DEFINED:", SITE, "Cylinder", str(i)+"_"+label)
                        continue
                    
                    print(i, label)
                    
                    # Load SPECT data and associated metadata
                    data_SPECT, voxelsize_SPECT, imagePosition_SPECT, imageOrientation_SPECT, headers_SPECT = \
                        transform_lib.loadData(path_SPECT, return_headers=True)

                    # Generate affine transformation matrices for SPECT data
                    A_SPECT, iA_SPECT = transform_lib.genDICOMAffine3D(
                        imagePosition_SPECT, imageOrientation_SPECT, voxelsize_SPECT
                    )

                    # Retrieve the frame of reference UID for SPECT data
                    FoR_SPECT = headers_SPECT.FrameOfReferenceUID

                    # Warn if frame of reference UIDs do not match between CT and SPECT
                    if FoR_CT != FoR_SPECT:
                        print("WARNING: Frame of reference UID mismatch between CT and SPECT.")
                        print("CT path:", path_CT)
                        print("SPECT path:", path_SPECT)

                    # Transform CT data to align with SPECT data
                    data_CT_transformed = cylinder_functions.transform_CT_to_SPECT(
                        A_SPECT, data_SPECT, iA_CT, data_CT, transSPECTtoCT
                    )

                    # Correct CT data values outside the reconstruction region (needed for GE data)
                    data_CT_transformed[data_CT_transformed < -1024] = -1024
                
                    # Filter CT data for smoothing
                    data_CT_transformed_fltr = register_masks.filter(
                        data_CT_transformed, voxelsize_SPECT, CT_filter_size
                    )

                    # Create a binary mask for the phantom
                    mask_CT_transformed = data_CT_transformed_fltr > threshold_HU[isite]

                    # Calculate and print the mask volume
                    mask_volume = np.prod(voxelsize_SPECT / 10) * np.sum(mask_CT_transformed)
                    print("CT mask volume (ml):", str(mask_volume))

                    # Calculate the center of mass for the mask
                    center_CT_transformed = center_of_mass(mask_CT_transformed)
                    coord_center_CT_transformed = [int(round(coord)) for coord in center_CT_transformed]

                    # Plot the CT and mask for the first protocol
                    if ilab == 0:
                        cylinder_functions.plot_CT_cylinder_mask(data_CT_transformed, mask_CT_transformed, data_SPECT, coord_center_CT_transformed, np.abs(voxelsize_SPECT), SITE + label)
                    
                    ### Extend the CT mask along the extended axis of the cylinder phantom
                    # Find the slices for which there is no mask
                    # First, erode the mask to make sure that it's not finding
                    # only caps to extend
                    ball_erode = morph.ball(6)
                    
                    mask_CT_transformed_erode = morphology.binary_erosion(mask_CT_transformed, structure=ball_erode)
                    mask_profile = mask_CT_transformed_erode.sum(axis=(1,2))
                    # find start and end slice of the mask
                    slices_mask = np.where(mask_profile>(0.95*mask_profile.max()))
                    
                    # dilate mask_CT_transformed to make sure that all counts in the radial direction are included 
                    ball_dilate = morph.ball(5)
                    mask_CT_transformed_dilate = morphology.binary_dilation(mask_CT_transformed, structure=ball_dilate)

                    # extend the dilated mask based on the start and end slices found using mask_CT_transformed_erode
                    mask_CT_transformed_dilate[0:slices_mask[0][0]-1,:,:] = mask_CT_transformed_dilate[slices_mask[0][0],:,:]
                    mask_CT_transformed_dilate[slices_mask[0][1]+1:-1,:,:] = mask_CT_transformed_dilate[slices_mask[0][1],:,:]
                    
                    # Mask the SPECT image 
                    data_SPECT_masked = np.multiply(data_SPECT,mask_CT_transformed_dilate)
                    
                    # Find the center of the masked SPECT data
                    coordCOM = cylinder_functions.get_center(data_SPECT_masked, voxelsize_SPECT[0])

                    # Initialize the sum of counts (profile in the axial direction)
                    axial_profile_sum = []                        

                    # Get the sum of the counts (profile) for the masked SPECT data
                    for slice in range(data_SPECT.shape[0]):
                        axial_profile_sum.append(data_SPECT_masked[slice].sum())

                    # Save data in the results YAML
                    res = results.setdefault(SITE, {})
                    res = res.setdefault(label, {})
                    res[i] = {
                        "Voxel size SPECT": [float(x) for x in voxelsize_SPECT],
                        "Center slice axial direction": round(coordCOM[0]),
                        "Sum of counts in slice": [float(x) for x in axial_profile_sum],
                    }
                                          
                    with open(RESULT_YAML, "w") as f:
                        yaml.dump(results, f, default_flow_style = None)
                        
                        
                        
      
