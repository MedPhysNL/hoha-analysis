"""
This script calculates the system sensitivity as presented in Table 3. The calculation 
is based on the projection data (raw SPECT data) and the activity present in the 
phantom at scan time (filled in below in `dataset`).

Key features:
- Sensitivity is calculated for each energy window by processing pixel data from the 
  DICOM files and normalizing it by acquisition parameters and activity levels.
- Results are visualized as scatter plots, with sensitivity plotted against activity for
  each energy window and imaging system.
- Supports multiple imaging systems, defined in the `dataset` dictionary, which contains
  file paths, activity values, and system names.

Dependencies:
- matplotlib: For generating plots.
- pydicom: For reading and processing DICOM files.

Output:
- A figure visualizing sensitivity for all systems across the defined energy windows.
- Sensitivity data is printed in the console for further inspection.
"""

# Import necessary libraries
import os  # For file and path operations
import matplotlib.pyplot as plt  # For creating and displaying plots
import pydicom                   # For handling and reading DICOM files


# Define a list of colors for plotting
colors = ['tab:red', 'tab:green', 'tab:blue', 'tab:orange', 
          'tab:purple', 'tab:cyan', 'tab:olive', 'tab:gray', 
          'tab:brown', 'tab:pink']

# Define labels for the energy windows used in the analysis
energyLabels = ["Low scatter", "Photopeak", "High scatter", "Upper scatter"]

# Define a dataset dictionary containing metadata for different centers
dataset = {
    "LUMC": {  # Leiden University Medical Center
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/LUMC/GE Discovery 670/Cylinder/1/A/Cylinder acqA/AcqA-cyl-1_EM001_DS.dcm",  # File path
        "act": 269,  # Activity value in MBq
        "name": "GE 670",  # Scanner name
        },
    "MUMC": {  # Maastricht University Medical Center
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/MUMC/GE Discovery 870/Cylinder/1/A/Cylinder acqA/TomoHo166A_Ho166001_DS.dcm",
        "act": 281,
        "name": "GE 870",
        },
    "NKI_Intevo": {  # Netherlands Cancer Institute, Intevo Scanner
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/NKI/Intevo/Cylinder/1/A/Cylinder acqA/2.25.31137335442364319976831584652639676085",
        "act": 266,  # Corrected activity on 2024-02-26
        "name": "Siemens Intevo 1",
        },
    "Radboud_Intevo": {  # Radboud University, Intevo Scanner
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/Radboud/Intevo/Cylinder/1/A/Cylinder acqA/HO_HARMONIZATION_Radboud(CylA)",
        "act": 284,
        "name": "Siemens Intevo 2",
        },
    "NKI_Symbia": {  # Netherlands Cancer Institute, Symbia Scanner
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/NKI/SymbiaT16/Cylinder/1/A/Cylinder acqA/2.25.32690274351576681031263024562787762312",
        "act": 277,  # Corrected activity on 2024-02-26
        "name": "Siemens Symbia 1",
        },
    "Radboud_Symbia": {  # Radboud University, Symbia Scanner
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/Radboud/SymbiaT16/Cylinder/1/A/Cylinder acqA/HO_HARMONIZATION_Radboud(CylA)",
        "act": 304,
        "name": "Siemens Symbia 2",
        },
    "UMCU": {  # University Medical Center Utrecht
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/UMCU/SymbiaT16/Cylinder/1/A/Cylinder acqA/HO_HARMONIZATION_UMCU.NM._.1000.0001.2022.01.30.07.56.51.572640.11855506.IMA",
        "act": 331,
        "name": "Siemens Symbia 3",
        },
}

# Function to calculate system sensitivity values for different energy windows
def get_sensitivity(fn, act, name):
    # Read the DICOM file specified by the file path
    ds = pydicom.read_file(fn, force=True)
    data = ds.pixel_array  # Extract the pixel data array from the DICOM file

    # Compute the acquisition time in seconds from the DICOM metadata
    count_time_s = ds.RotationInformationSequence[0].ActualFrameDuration / 1000
    # Calculate the number of projections per energy window
    numProjections = int(ds.NumberOfFrames / ds.NumberOfEnergyWindows)
  
    # Retrieve the energy window sequence from the DICOM metadata
    ew = ds.EnergyWindowInformationSequence
    for i, ewin in enumerate(ew):  # Iterate through each energy window
        rs = ewin.EnergyWindowRangeSequence
        for r in rs:  # Iterate through energy ranges within each window
            low = r.EnergyWindowLowerLimit
            high = r.EnergyWindowUpperLimit
            # Categorize the data based on the energy range
            if 70 > low and 70 < high:
                sc_low = data[i*numProjections:(i+1)*numProjections]  # Low scatter
            if 81 > low and 81 < high:
                pp = data[i*numProjections:(i+1)*numProjections]  # Photopeak
            if 90 > low and 90 < high:
                sc_high = data[i*numProjections:(i+1)*numProjections]  # High scatter
            if 118 > low and 118 < high:
                sc_upper = data[i*numProjections:(i+1)*numProjections]  # Upper scatter

    # Calculate sensitivity for each energy window
    sens_low_whole = sc_low.sum() / (numProjections * count_time_s * act )
    sens_pp_whole = pp.sum() / (numProjections * count_time_s * act )
    sens_high_whole = sc_high.sum() / (numProjections * count_time_s * act )
    sens_upper_whole = sc_upper.sum() / (numProjections * count_time_s * act )

    # Round the sensitivity values for readability
    data_wholeProjection_round = [name, round(sens_low_whole, 2), 
                                   round(sens_pp_whole, 2), 
                                   round(sens_high_whole, 2), 
                                   round(sens_upper_whole, 2)]

    # Print the sensitivity values for debugging
    print("Whole projection")
    print(round(sens_low_whole, 2))
    print(round(sens_pp_whole, 2))
    print(round(sens_high_whole, 2))
    print(round(sens_upper_whole, 2))  
    
    # Return the rounded sensitivity values
    return data_wholeProjection_round

# Main script execution
if __name__ == "__main__":
    # Initialize a table to store sensitivity data for all centers
    table_fullProjections = []

    # Create a figure and axes for plotting
    fig_wholeProjection, ax_wholeProjection = plt.subplots(1, 4, figsize=(10, 3.5), sharey=True)

    icenter = 0  # Initialize index for color selection
    for center, center_data in dataset.items():  # Iterate through each center
        print(center)  # Print the center name
        # Calculate sensitivity data for the current center
        data_wholeProjection = get_sensitivity(center_data["fn"], center_data["act"], center_data["name"])
        # Append the calculated data to the table
        table_fullProjections.append(data_wholeProjection)
        print()

        # Plot the sensitivity data for each energy window
        for energyWindow in range(len(data_wholeProjection) - 1):
            ax_wholeProjection[energyWindow].plot(center_data["act"], 
                                                  data_wholeProjection[energyWindow + 1], 
                                                  'o', label=center_data["name"], 
                                                  color=colors[icenter])
            # Set labels, titles, and grid for the subplot
            ax_wholeProjection[energyWindow].set_xlabel("Activity [MBq]")
            ax_wholeProjection[energyWindow].set_title(energyLabels[energyWindow])
            ax_wholeProjection[energyWindow].set_ylim(bottom=0, top=9)
            ax_wholeProjection[energyWindow].grid()
            
            # Add a y-axis label to the first subplot
            if energyWindow == 0:
                ax_wholeProjection[energyWindow].set_ylabel("Sensitivity [cps/MBq]")
            # Add a legend to the last subplot
            if energyWindow == 3:
                ax_wholeProjection[energyWindow].legend(loc='upper right', 
                                                        fancybox=True, 
                                                        bbox_to_anchor=(2.40, 0.95))
        
        icenter += 1  # Increment the color index

    # Set the overall title for the figure
    fig_wholeProjection.suptitle("Whole projection")
    # Draw and display the plot
    plt.draw()
    plt.show()
    
    # Save the plot as a PNG image
    fn_wholeProjection = "Sensitivity_wholeProjection.png"
    # Uncomment to save: fig_wholeProjection.savefig(os.path.join("Figures", fn_wholeProjection), dpi=500, orientation='landscape', format='png', bbox_inches='tight')
