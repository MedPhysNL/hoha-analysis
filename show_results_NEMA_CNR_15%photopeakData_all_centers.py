"""
This script analyzes the contrast-to-noise ratio (CNR) of the NEMA phantom.
The plot is presented as Figure 6 and the data printed in the console is presented in Table 6.
The script performs the following:
- Loads pre-computed results and configuration data from YAML files.
- Averages the data over the number of acqusitions.
- Plots the data.
- Prints the mean+-SD (range) to the console

Modules and Libraries:
- Core modules: `yaml`, `numpy`, `matplotlib`.
- Custom modules: `register_masks`

Output:
- A plot visualizing the CNRs versus the insert diameters, per reconstruction protocol
- Mean and standard deviation (range) is printed to the console
"""
# Import necessary libraries
import os  # For file and path operations
import yaml  # For reading configuration and results stored in YAML files
import numpy as np  # For numerical operations and array manipulations
import matplotlib.pyplot as plt  # For generating and displaying plots
import register_masks  # Custom module for sphere diameters


plt.rcParams.update({'font.size': 16})

# Define the imaging sites for analysis
SITES = [   "LUMC Discovery670",
            "MUMC Discovery870",
            "NKI Intevo",
            "Radboud Intevo",
            "NKI SymbiaT16",
            "Radboud SymbiaT16",
            "UMCU SymbiaT16",   
            ]

# Corresponding scanner IDs to be used as legend
LEGEND = ["GE 670",
        "GE 870",
        "Siemens Intevo 1",
        "Siemens Intevo 2",
        "Siemens Symbia 1",
        "Siemens Symbia 2",
        "Siemens Symbia 3",
        ]

marker_sphere = ['o-','v-','^-','<-','>-','s-','P-']
marker_lung = ['o','v','^','<','>','s','P']


# Labels for reconstruction protocols and their corresponding plot labels
LABELS = ["AcqA_DEW", "AcqA_TEW", "AcqA_HER"]
LABELS_PLOT = ["DEW", "TEW", "MC"]

# Select which filtered data is to be plotted (0 filter means no filtering)
# FILTER_SIZE_MM = [0, 5, 7.5, 10, 15]
FILTER_SIZE_MM = [0]

# Define the x-axis position the data for the lung insert will be plotted at
lungDiameter_mm = 50

# Set how many measurements to expect (for initializing data arrays)
numRepeatedMeas = 3

colors = ['tab:red', 'tab:green', 'tab:blue', 'tab:orange', 'tab:purple', 'tab:cyan', 'tab:olive', 'tab:gray', 'tab:brown', 'tab:pink']

# Load results and configuration data from YAML files
with open("results_NEMA.yml", "r") as f:
    results = yaml.safe_load(f)
with open("config_data.yml", "r") as f:
    conf = yaml.safe_load(f)

# Helper function to load results for a specific site and reconstruction method
def load_results(site, filter_size, label):
    all_CRCs = []
    all_lCRC = []
    all_CNRs = []
    all_lCNR = []
    all_COV = []
    
    for i in [1,2,3]:
        try:
            res = results[site][filter_size][label][i]
        except KeyError:
            print("NOT DEFINED:", site, "NEMA", str(i)+"_"+label)
            continue
        
        fill_ratio = conf["Site data"][site]["NEMA"]["Fill_Ratio"]
        sphere_means = res["sphere_means"]
        bgr_mean = res["bgr_mean"]
        bgr_std = res["bgr_std"]
        lung_mean = res["lung_mean"]
        
        CRCs = [(sph_mean/bgr_mean-1)/(fill_ratio-1) for sph_mean in sphere_means]            
        lCRC = 1-lung_mean/bgr_mean
        COV = bgr_std/bgr_mean
        
        CNRs = [(sph_mean-bgr_mean)/bgr_std for sph_mean in sphere_means]  
        lCNR = (lung_mean-bgr_mean)/bgr_std
        
        all_CRCs.append(CRCs)
        all_lCRC.append(lCRC)
        all_CNRs.append(CNRs)
        all_lCNR.append(lCNR)
        all_COV.append(COV)
        
    
    if not len(all_CRCs):
        return None, None, None, None, None, None, None, None, None, None
    
    av_CRCs = np.array(all_CRCs).mean(axis=0)
    av_lCRC = np.mean(all_lCRC)
    av_CNRs = np.array(all_CNRs).mean(axis=0)
    av_lCNR = np.mean(all_lCNR)
    av_COV = np.mean(all_COV)
    
    return av_CRCs, av_lCRC, av_CNRs, av_lCNR, av_COV, np.array(all_CRCs), np.array(all_lCRC), np.array(all_CNRs), np.array(all_lCNR), np.array(all_COV)

mm_to_inch = 0.0393701
# Loop over FILTER_SIZE_MM, create one plot per filter size
for ifilter, filter_size in enumerate(FILTER_SIZE_MM):
    print("Ploting results for filter size: " + str(filter_size) + " mm")
    fig_CNR, ax_CNR = plt.subplots(1, len(LABELS), figsize = (500*mm_to_inch, 200*mm_to_inch), sharey = True)

    # make arrays that holds all the mean CNR information
    all_av_CNRs = np.zeros([len(LABELS)*len(SITES),6]) 
    all_av_CNRs[:] = np.nan
    all_av_lCNR = np.zeros([len(LABELS)*len(SITES),1]) 
    all_av_lCNR[:] = np.nan
    
    # Loop over each reconstruction protocol (label)
    for ilab, label in enumerate(LABELS):
        print("Reconstruction: " + label)
        # make arrays that holds all the CNR information, is regenerated for each new label
        all_meas_CNRs = np.zeros([len(SITES)*numRepeatedMeas,6]) 
        all_meas_CNRs[:] = np.nan
        all_meas_lCNR = np.zeros([len(SITES)*numRepeatedMeas,1]) 
        all_meas_lCNR[:] = np.nan

        # Loop over each site
        for isite, site in enumerate(SITES):
            av_CRCs, av_lCRC, av_CNRs, av_lCNR, av_COV, all_CRCs, all_lCRC, all_CNRs, all_lCNR, all_COV  = load_results(site, filter_size, label)       
            
            if not (av_CNRs is None):    
                index_av = (ilab*len(SITES))+isite
                index = (isite*(numRepeatedMeas-1))+isite
                all_av_CNRs[index_av,:] = av_CNRs
                all_av_lCNR[index_av,:] = av_lCNR    

                all_meas_CNRs[index:(index+all_CNRs.shape[0]),:] = all_CNRs
                all_meas_lCNR[index:(index+all_lCNR.shape[0]),:] = all_lCNR.reshape([all_lCNR.shape[0],1])            
                
                ax_CNR[ilab].plot(register_masks.SPHERES_MM, av_CNRs, marker_sphere[isite], label=LEGEND[isite], color = colors[isite]) 
                
                ax_CNR[ilab].plot(lungDiameter_mm, av_lCNR, marker_lung[isite], color = colors[isite]) 
                
            else: # plot made-up data outside of the visible graph to make sure that all SITES are displayed in the legend
                ax_CNR[ilab].plot(register_masks.SPHERES_MM, [-1000,-1000,-1000,-1000,-1000,-1000], 'o-', label=LEGEND[isite], color = colors[isite])

            if (ilab == 0) and (isite == 0):
                ax_CNR[ilab].set_ylabel("Contrast-to-noise ratio", weight='bold')
                
            if (ilab == np.floor(len(LABELS)/2)) and (isite == 0):
                ax_CNR[ilab].set_xlabel("Insert diameter [mm]", weight='bold')
   
        ax_CNR[ilab].set_xlim(left=0)
        ax_CNR[ilab].set_ylim(bottom=-4, top=22)
        ax_CNR[ilab].grid()
        
        label = LABELS_PLOT[ilab]
        ax_CNR[ilab].set_title(label, weight='bold')

        print(" ")
        print(label)
        # this is mean of ALL measurements (of the same label)
        print("Contrast-to-noise ratios")
          
        print("Min CNRs")
        minCNRs = np.nanmin(all_meas_CNRs,0)
        print(np.round(minCNRs,2))
        
        print("Max CNRs")
        maxCNRs = np.nanmax(all_meas_CNRs,0)
        print(np.round(maxCNRs,2))
        
        print("Mean CNRs, over all scanners")
        meanCNRs = np.nanmean(all_meas_CNRs,0)
        print(np.round(meanCNRs,2))
        
        print("SD CNRs, over all scanners")
        stdCNRs = np.nanstd(all_meas_CNRs,0)
        print(np.round(stdCNRs,2))
        
        for isphere in range(len(meanCNRs)):
            print(str(np.round(meanCNRs[isphere],2)) + u" \u00B1 " + str(np.round(stdCNRs[isphere],2)) + " (" + str(np.round(minCNRs[isphere],2)) + " - " + str(np.round(maxCNRs[isphere],2)) + ")")
        
        print("Min lCNR")
        minlCNR = np.nanmin(all_meas_lCNR,0)
        print(np.round(minlCNR,2))
        print("Max lCNR")
        maxlCNR = np.nanmax(all_meas_lCNR,0)
        print(np.round(maxlCNR,2))
        
        print("Mean lCNR, over all scanners")
        meanlCNR = np.nanmean(all_meas_lCNR,0)
        print(np.round(meanlCNR,2))
        
        print("SD lCNR, over all scanners")
        stdlCNR = np.nanstd(all_meas_lCNR,0)
        print(np.round(stdlCNR,2))
        
        print(" ") 
        

    ax_CNR[0].legend(loc=2)           
    fig_CNR.suptitle('filter FWHM ' + str(filter_size))
    
    # Set the figure name
    fn_figure_CNR = 'NEMA_15%photopeak_CNRs with lung filter FWHM ' + str(filter_size) + ' mm.png'
    fig_CNR.subplots_adjust(wspace=0.1, hspace=0)
    # Save the figure

    fig_CNR.savefig(os.path.join("NEMA_figures", fn_figure_CNR), dpi=600, orientation='landscape',format='png',  bbox_inches='tight')
      
    plt.draw()
    plt.pause(2)
    plt.close()
