"""
This script contains functions (used by other scripts) for processing and visualizing 
cylindrical phantoms in medical imaging. 
It includes functions to create a circular mask, determine the center of a cylinder slice, transform 
CT data to match SPECT data, and plot the results.

Modules and libraries used:
- Libraries: `numpy`, `scipy`, `matplotlib`, `pydicom`.
"""
# Import necessary libraries and modules
import os  # For file path operations
import numpy as np  # For numerical operations
from scipy.ndimage import center_of_mass, gaussian_filter, zoom, affine_transform  # For image processing
import matplotlib.pyplot as plt  # For plotting and visualization
import pydicom # For reading DICOM data

# Returns a 2D grid with a logical mask for the cross-section of the cylindrical phantom
# radius in number of voxels
def get_circlemask(shape, center, radius):
    dy, dx = shape
    cy, cx = center
    y, x = np.ogrid[-cy:dy-cy, -cx:dx-cx]
    return ((x/radius)**2 + (y/radius)**2 <= 1)

# Returns the center coordinate for the cylinder slice by thresholding a filtered version
# of the SPECT data (slice). Voxels with a value higher than 20% of the maximum value
# are saved in mask. The original SPECT image is masked with mask and then filtered
# again to create a new mask on which the final cylinder mask is based.
def get_center(img, pixsize):
    # Step 1: Get rough idea of phantom
    blr = gaussian_filter(img, 20/pixsize)
    mask = blr > 0.2 * blr.max()
    
    # Step 2: Remove all the edge/scatter pixels
    img2 = np.copy(img)
    img2[~mask] = 0
    
    # Step 3: Repeat step 1
    blr = gaussian_filter(img2, 20/pixsize)
    mask = blr > 0.2 * blr.max()
    
    return center_of_mass(mask)

# Transforms CT data to match SPECT data using affine transformation
def transform_CT_to_SPECT(A_SPECT, data_SPECT, iA_CT, data_CT, trans):
    A_TR = iA_CT @ trans @ A_SPECT
    return affine_transform(data_CT, A_TR, output_shape=data_SPECT.shape, order=1, cval=-1000)

# Plots the CT and SPECT data with the cylindrical mask applied
def plot_CT_cylinder_mask(data_CT, mask_CT, data_SPECT_transformed, coord_center_CT, voxelsize_CT, plot_label):
    colormap_CT = 'gray'
    colormap_SPECT = 'hot'
    interp = 'none'
    mm_to_inch = 0.0393701
    fig_CT = plt.figure(figsize=(450 * mm_to_inch, 250 * mm_to_inch))
    
    data_CT_masked = np.multiply(data_CT, mask_CT)
    data_SPECT_transformed_masked = np.multiply(data_SPECT_transformed, mask_CT)
    
    aspect_ratio_xyplane = voxelsize_CT[1] / voxelsize_CT[2]
    aspect_ratio_xzplane = voxelsize_CT[0] / voxelsize_CT[2]
    aspect_ratio_yzplane = voxelsize_CT[0] / voxelsize_CT[1]
    
    # Plotting CT data
    ax = fig_CT.add_subplot(3, 4, 1)
    plt.imshow(data_CT[coord_center_CT[0], :, :], interpolation=interp, cmap=colormap_CT)
    plt.plot(coord_center_CT[2], coord_center_CT[1], 'r+')
    ax.set_title("CT")
    ax.set_ylabel("Axial")
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_xyplane)
    
    ax = fig_CT.add_subplot(3, 4, 5)
    plt.imshow(data_CT[:, coord_center_CT[1], :], interpolation=interp, cmap=colormap_CT)
    plt.plot(coord_center_CT[2], coord_center_CT[0], 'r+')
    ax.set_ylabel("Sagittal")
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_xzplane)
    
    ax = fig_CT.add_subplot(3, 4, 9)
    plt.imshow(data_CT[:, :, coord_center_CT[2]], interpolation=interp, cmap=colormap_CT)
    plt.plot(coord_center_CT[1], coord_center_CT[0], 'r+')
    ax.set_ylabel("Coronal")
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_yzplane)
    
    # Plotting masked CT data
    ax = fig_CT.add_subplot(3, 4, 2)
    ax.set_title("CT masked")
    plt.imshow(data_CT_masked[coord_center_CT[0], :, :], interpolation=interp, cmap=colormap_CT)
    plt.plot(coord_center_CT[2], coord_center_CT[1], 'r+')
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_xyplane)
    
    ax = fig_CT.add_subplot(3, 4, 6)
    plt.imshow(data_CT_masked[:, coord_center_CT[1], :], interpolation=interp, cmap=colormap_CT)
    plt.plot(coord_center_CT[2], coord_center_CT[0], 'r+')
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_xzplane)
    
    ax = fig_CT.add_subplot(3, 4, 10)
    plt.imshow(data_CT_masked[:, :, coord_center_CT[2]], interpolation=interp, cmap=colormap_CT)
    plt.plot(coord_center_CT[1], coord_center_CT[0], 'r+')
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_yzplane)
    
    # Plotting SPECT data
    ax = fig_CT.add_subplot(3, 4, 3)
    ax.set_title("SPECT")
    plt.imshow(data_SPECT_transformed[coord_center_CT[0], :, :], interpolation=interp, cmap=colormap_SPECT)
    plt.plot(coord_center_CT[2], coord_center_CT[1], 'b+')
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_xyplane)
    
    ax = fig_CT.add_subplot(3, 4, 7)
    plt.imshow(data_SPECT_transformed[:, coord_center_CT[1], :], interpolation=interp, cmap=colormap_SPECT)
    plt.plot(coord_center_CT[2], coord_center_CT[0], 'b+')
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_xzplane)
    
    ax = fig_CT.add_subplot(3, 4, 11)
    plt.imshow(data_SPECT_transformed[:, :, coord_center_CT[2]], interpolation=interp, cmap=colormap_SPECT)
    plt.plot(coord_center_CT[1], coord_center_CT[0], 'b+')
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_yzplane)
    
    # Plotting masked SPECT data
    ax = fig_CT.add_subplot(3, 4, 4)
    ax.set_title("SPECT masked")
    plt.imshow(data_SPECT_transformed_masked[coord_center_CT[0], :, :], interpolation=interp, cmap=colormap_SPECT)
    plt.plot(coord_center_CT[2], coord_center_CT[1], 'b+')
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_xyplane)
    
    ax = fig_CT.add_subplot(3, 4, 8)
    plt.imshow(data_SPECT_transformed_masked[:, coord_center_CT[1], :], interpolation=interp, cmap=colormap_SPECT)
    plt.plot(coord_center_CT[2], coord_center_CT[0], 'b+')
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_xzplane)
    
    ax = fig_CT.add_subplot(3, 4, 12)
    plt.imshow(data_SPECT_transformed_masked[:, :, coord_center_CT[2]], interpolation=interp, cmap=colormap_SPECT)
    plt.plot(coord_center_CT[1], coord_center_CT[0], 'b+')
    gca = plt.gca()
    gca.set_aspect(aspect_ratio_yzplane)
    
    fig_CT.suptitle("Masking based on CT, " + plot_label)
    fig_CT.tight_layout()
    plt.show()