import os
import pydicom as dicom
import numpy as np
from scipy.ndimage import affine_transform
from scipy.spatial.transform import Rotation
import scipy.ndimage
import matplotlib.pyplot as plt
import copy
import subprocess

try:
    import SimpleITK as sitk
except ImportError:
    print("Elastix transform not available. Install sitk")


def loadFolder(folder):
    data = []
    headers = []
    position = []
    for fn in os.listdir(folder):
        path = os.path.join(folder, fn)
        ds = dicom.read_file(path)
        headers.append(dicom.read_file(path, stop_before_pixels=True))
        data.append(ds.pixel_array.astype(float) * ds.RescaleSlope + float(ds.RescaleIntercept))
        position.append(float(ds.ImagePositionPatient[2]))
    position = np.array(position)
    sorting = np.argsort(position)
    position = position[sorting]
    data = np.array(data)[sorting]
    headers = [headers[s] for s in sorting]
    pixsize = list(map(float, ds.PixelSpacing)) + [position[1]-position[0]]
    return data, headers, np.array(pixsize)


def loadData( path, return_headers=False ):
    if os.path.isdir(path) and len(os.listdir(path)) > 1:
        data, headers, size = loadFolder( path )
        pos = np.array(headers[0].ImagePositionPatient)
        ori = np.array(headers[0].ImageOrientationPatient)
    else:
        if os.path.isdir(path):
            path = os.path.join(path, os.listdir(path)[0])
            
        ds = dicom.read_file( path )
        headers = dicom.read_file(path, stop_before_pixels=True)
        data = ds.pixel_array
        size = np.array(list(map(float, ds.PixelSpacing)) + [ds.SpacingBetweenSlices])
        pos = np.array(list(map(float, ds.DetectorInformationSequence[0].ImagePositionPatient)))
        ori = np.array(list(map(float, ds.DetectorInformationSequence[0].ImageOrientationPatient)))
    
    if return_headers:
        return data, size, pos, ori, headers
    else:
        return data, size, pos, ori
    
def loadDataRaw( path, return_headers=False ):
   
    if os.path.isdir(path):
        path = os.path.join(path, os.listdir(path)[0])
        
    ds = dicom.read_file( path )
    headers = dicom.read_file(path, stop_before_pixels=True)
    data = ds.pixel_array

    if return_headers:
        return data, headers
    else:
        return data


def genDICOMAffine3D(ImPos, ImOrient, PixelSize):
    vx = ImOrient[:3] * PixelSize[0]
    vy = ImOrient[3:] * PixelSize[1]
    vz = np.cross(ImOrient[:3], ImOrient[3:]) * PixelSize[2]
    
    A = np.zeros((4,4))
    A[:3, 0] = vz[::-1]
    A[:3, 1] = vy[::-1]
    A[:3, 2] = vx[::-1]
    A[:3, 3] = ImPos[::-1]
    A[3,3] = 1    
    return A, np.linalg.inv(A)


def DCM_to_ITK(data_directory):
    series_IDs = sitk.ImageSeriesReader.GetGDCMSeriesIDs(data_directory)
    
    if not series_IDs:
        print(f"ERROR: given directory {data_directory} does not contain a DICOM series.")
        exit()
    series_file_names = sitk.ImageSeriesReader.GetGDCMSeriesFileNames(data_directory, series_IDs[0])
    series_reader = sitk.ImageSeriesReader()
    series_reader.SetFileNames(series_file_names)
    series_reader.MetaDataDictionaryArrayUpdateOn()
    series_reader.LoadPrivateTagsOn()
    image3D = series_reader.Execute()    
    return image3D


def runElastix(fixed_series, moving_series, out_folder):
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    
    fixed = os.path.join(out_folder, "fixedImage.mhd")
    moving = os.path.join(out_folder, "movingImage.mhd")
    params = os.path.join(out_folder, "parameters.txt")
    transform_file = os.path.join(out_folder, "TransformParameters.0.txt")
    
    with open(params, "w") as f:
        f.write(PARAMETERS_RIGID)
    
    fixedImage = DCM_to_ITK(fixed_series)
    movingImage = DCM_to_ITK(moving_series)

    sitk.WriteImage(fixedImage, fixed)
    sitk.WriteImage(movingImage, moving)

    args = ['elastix', '-f', fixed, '-m', moving, '-out', out_folder, '-p', params]

    subprocess.run(args)    
    
    return getElastixTransformationMatrix(transform_file)


def getElastixTransformationMatrix(filename):
    with open(filename, "r") as f:
        params = f.readlines()
        
        for line in params:
            line = line.replace("(","").replace(")","")
            
            if line.startswith("TransformParameters"):    
                transform = [float(x) for x in line.split()[1:]]
                euler = np.array(transform[:3])
                trans = np.array(transform[3:])
                affine = np.array(transform)
                if len(affine) == 12:
                    aff = np.identity(4)
                    aff[0,:3] = affine[:3]
                    aff[1,:3] = affine[3:6]
                    aff[2,:3] = affine[6:9]
                    aff[:3,3] = affine[9:]                    
        
            if line.startswith("CenterOfRotationPoint"):
                center = [float(x) for x in line.split()[1:]]
                center = np.array(center)
    
    
    cmat = np.identity(4)
    cmat[:3,3] = center[::-1]
    cmat_inv = np.linalg.inv(cmat)
    
    r = Rotation.from_euler('zyx', -euler, degrees=False)
    TR = np.identity(4)    
    TR[:3,:3] = r.as_matrix()
    TR[:3,3] = trans[::-1]
    
    tr_lps_zyx = cmat @ TR @ cmat_inv
    return tr_lps_zyx


def showResult(ref, out, slicenr, n_squares=10):    
    n = out.shape[1]
    i = int(np.ceil(n/float(n_squares)))
    arr = np.zeros((n_squares,n_squares),dtype=int)
    arr[::2,::2] = 1
    arr[1::2,1::2] = 1
    check = np.kron(arr, np.ones((i,i)))[:n,:n]
    cmap = copy.copy(plt.cm.get_cmap("hot"))
    cmap.set_bad(alpha=0)
    out_masked = np.ma.masked_where(check, out[slicenr])
    
    plt.imshow(ref[slicenr], cmap='gray')
    plt.imshow(out_masked, cmap=cmap)

    plt.show()




PARAMETERS_RIGID = """// Example parameter file for rotation registration
// C-style comments: //

// The internal pixel type, used for internal computations
// Leave to float in general. 
// NB: this is not the type of the input images! The pixel 
// type of the input images is automatically read from the 
// images themselves.
// This setting can be changed to "short" to save some memory
// in case of very large 3D images.
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")

// The dimensions of the fixed and moving image
// Up to elastix 4.5 this had to be specified by the user.
// From elastix 4.6, this is not necessary anymore.
//(FixedImageDimension 2)
//(MovingImageDimension 2)

// Specify whether you want to take into account the so-called
// direction cosines of the images. Recommended: true.
// In some cases, the direction cosines of the image are corrupt,
// due to image format conversions for example. In that case, you 
// may want to set this option to "false".
(UseDirectionCosines "true")

// **************** Main Components **************************

// The following components should usually be left as they are:
(Registration "MultiResolutionRegistration")
(Interpolator "BSplineInterpolator")
(ResampleInterpolator "FinalBSplineInterpolator")
(Resampler "DefaultResampler")

// These may be changed to Fixed/MovingSmoothingImagePyramid.
// See the manual.
(FixedImagePyramid "FixedSmoothingImagePyramid")
(MovingImagePyramid "MovingSmoothingImagePyramid")

// The following components are most important:
// The optimizer AdaptiveStochasticGradientDescent (ASGD) works
// quite ok in general. The Transform and Metric are important
// and need to be chosen careful for each application. See manual.
(Optimizer "AdaptiveStochasticGradientDescent")
(Transform "EulerTransform")
(Metric "AdvancedNormalizedCorrelation")

// ***************** Transformation **************************

// Scales the rotations compared to the translations, to make
// sure they are in the same range. In general, it's best to  
// use automatic scales estimation:
(AutomaticScalesEstimation "true")

// Automatically guess an initial translation by aligning the
// geometric centers of the fixed and moving.
(AutomaticTransformInitialization "true")

// Whether transforms are combined by composition or by addition.
// In generally, Compose is the best option in most cases.
// It does not influence the results very much.
(HowToCombineTransforms "Compose")

// Added this myself. Ratio of fixed images samples that should have a correspondence. Default 0.25
(RequiredRatioOfValidSamples 0.05)

// ******************* Similarity measure *********************

// Number of grey level bins in each resolution level,
// for the mutual information. 16 or 32 usually works fine.
// You could also employ a hierarchical strategy:
//(NumberOfHistogramBins 16 32 64)
(NumberOfHistogramBins 32)

// If you use a mask, this option is important. 
// If the mask serves as region of interest, set it to false.
// If the mask indicates which pixels are valid, then set it to true.
// If you do not use a mask, the option doesn't matter.
(ErodeMask "true")

// ******************** Multiresolution **********************

// The number of resolutions. 1 Is only enough if the expected
// deformations are small. 3 or 4 mostly works fine. For large
// images and large deformations, 5 or 6 may even be useful.
(NumberOfResolutions 4)

// The downsampling/blurring factors for the image pyramids.
// By default, the images are downsampled by a factor of 2
// compared to the next resolution.
// So, in 2D, with 4 resolutions, the following schedule is used:
//(ImagePyramidSchedule 8 8  4 4  2 2  1 1 )
// And in 3D:
//(ImagePyramidSchedule 8 8 8  4 4 4  2 2 2  1 1 1 )
// You can specify any schedule, for example:
//(ImagePyramidSchedule 4 4  4 3  2 1  1 1 )
// Make sure that the number of elements equals the number
// of resolutions times the image dimension.

// ******************* Optimizer ****************************

// Maximum number of iterations in each resolution level:
// 200-500 works usually fine for rigid registration.
// For more robustness, you may increase this to 1000-2000.
(MaximumNumberOfIterations 250)

// The step size of the optimizer, in mm. By default the voxel size is used.
// which usually works well. In case of unusual high-resolution images
// (eg histology) it is necessary to increase this value a bit, to the size
// of the "smallest visible structure" in the image:
(MaximumStepLength 1.0)

// **************** Image sampling **********************

// Number of spatial samples used to compute the mutual
// information (and its derivative) in each iteration.
// With an AdaptiveStochasticGradientDescent optimizer,
// in combination with the two options below, around 2000
// samples may already suffice.
(NumberOfSpatialSamples 2048)

// Refresh these spatial samples in every iteration, and select
// them randomly. See the manual for information on other sampling
// strategies.
(NewSamplesEveryIteration "true")
(ImageSampler "Random")


// ************* Interpolation and Resampling ****************

// Order of B-Spline interpolation used during registration/optimisation.
// It may improve accuracy if you set this to 3. Never use 0.
// An order of 1 gives linear interpolation. This is in most 
// applications a good choice.
(BSplineInterpolationOrder 1)

// Order of B-Spline interpolation used for applying the final
// deformation.
// 3 gives good accuracy; recommended in most cases.
// 1 gives worse accuracy (linear interpolation)
// 0 gives worst accuracy, but is appropriate for binary images
// (masks, segmentations); equivalent to nearest neighbor interpolation.
(FinalBSplineInterpolationOrder 3)

//Default pixel value for pixels that come from outside the picture:
(DefaultPixelValue 0)

// Choose whether to generate the deformed moving image.
// You can save some time by setting this to false, if you are
// only interested in the final (nonrigidly) deformed moving image
// for example.
(WriteResultImage "false")

// The pixel type and format of the resulting deformed moving image
(ResultImagePixelType "short")
(ResultImageFormat "mhd")"""









    
if __name__ == "__main__":
    fRef = "CT" #This could be a folder with CT slices
    fIn = "recon.IMA" #This could be a SPECT recon
    
    dIn, sIn, pIn, oIn = loadData( fIn )
    dRef, sRef, pRef, oRef = loadData( fRef )

    A_In, iA_In = genDICOMAffine3D(pIn, oIn, sIn)
    A_REF, iA_REF = genDICOMAffine3D(pRef, oRef, sRef)
    A_TR = np.dot(iA_In, A_REF)

    dOut = affine_transform(dIn, A_TR, output_shape=dRef.shape, order=0)
    
    showResult(dRef, dOut, len(dOut)//2)


