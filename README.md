This folder contains the code needed to create the plots and results for "Towards harmonized holmium-166 SPECT image quality for dosimetry: a multi-center, multi-vendor study" (DOI: TO BE FILLED IN).

The code has been written in Python 3.9. 

All (intermediate) results are available. The data can be made available upon reasonable request, contact the corresponding author for this. 



To run the analysis for new data, add the paths to the data in the config-files and run the analysis_data- and show_results-scripts of interest.



General information:

	Paths to all imaging data are set in:

		- config_data.yml and 

		- config_data_NEMA_convergence.yml 

	

	Results from the analyses are save in:

		- results_cylinder_COV.yml

		- results_cylinder_uniformity.yml

		- results_NEMA.yml

		- results_NEMA_convergence.yml



	In the scripts:

	- AcqA refers to the 15% wide photopeak window data (main text)

	- AcqB refers to the 20% wide photopeak window data (supplemental information)





Which scripts are used to generate which figures/tabels?



	Main text:

	- Table 3:

		- analyze_data_sensitivity_15%photopeakData.py

	- Figure 4:

		- config_data.yml

		- results_cylinder_uniformity.yml

		- analyze_data_cylinder_uniformity.py

		- show_results_cylinder_uniformity_15%photopeakData.py

	- Table 4:

		- config_data.yml

		- analyze_data_cylinder_COV.py

		- results_cylinder_COV.yml

	- Figure 5, Table 5:

		- config_data.yml

		- results_NEMA.yml

		- analyze_data_NEMA.py

		- show_results_NEMA_CRC_15%photopeakData_all_centers.py

	- Figure 6, Table 6:

		- config_data.yml

		- results_NEMA.yml

		- analyze_data_NEMA.py

		- show_results_NEMA_CNR_15%photopeakData_all_centers.py



	Supplementary information:

	- Table S.1:

		- config_data.yml

		- results_NEMA.yml

		- analyze_data_NEMA.py

		- show_results_NEMA_CRC_15%photopeakData_all_centers.py

	- Table S.2:

		- config_data.yml

		- results_NEMA.yml

		- analyze_data_NEMA.py

		- show_results_NEMA_CNR_15%photopeakData_all_centers.py

	- Table S.3:

		- analyze_data_sensitivity_20%photopeakData.py

	- Figure S.2:

		- config_data.yml

		- results_cylinder_uniformity.yml

		- analyze_data_cylinder_uniformity.py

		- show_results_cylinder_uniformity_20%photopeakData.py

	- Table S.4:

		- config_data.yml

		- analyze_data_cylinder_COV.py

		- results_cylinder_COV.yml

	- Figure S.3, Table S.5, Table S.6:

		- config_data.yml

		- results_NEMA.yml

		- analyze_data_NEMA.py

		- show_results_NEMA_CRC_20%photopeakData_all_centers.py

	- Figure S.4, Table S.7, Table S.8:

		- config_data.yml

		- results_NEMA.yml

		- analyze_data_NEMA.py

		- show_results_NEMA_CNR_20%photopeakData_all_centers.py

	- Figure S.5:

		- config_data_NEMA_convergence.yml

		- results_NEMA_convergence.yml

		- analyze_data_NEMA_convergence.py

		- show_results_NEMA_convergence.py

	- Figure S.6 to S.12:

		- config_data.yml

		- results_NEMA.yml

		- analyze_data_NEMA.py

		- show_results_NEMA_CRC_filter_per_center.py

	- Figure S.13 to S.19:

		- config_data.yml

		- results_NEMA.yml

		- analyze_data_NEMA.py

		- show_results_NEMA_CNR_filter_per_center.py



Scripts for dependencies / functions / supporting libraries:

	- cylinder_functions.py

	- define_vois.py

	- elastix_transforms.pickle

	- NEMA_VOIs.pickle

	- register_masks.py

	- transform_lib.py