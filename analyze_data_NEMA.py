"""
This script analyzes NEMA data from various imaging sites. It processes CT and SPECT images,
registers them, applies filters, and calculates metrics such as sphere means, background mean,
background standard deviation, and lung mean. The results are saved in a YAML file.

Modules and libraries used:
- Custom modules: `define_vois`,`register_masks`, `transform_lib`.

Output:
- Detailed results are stored in `results_NEMA.yml`.
- Warnings and intermediate values are printed to the console.
"""

# Import necessary libraries and modules
import os  # For file path operations
import yaml  # For reading and writing YAML configuration and results files
import pickle # For saving and loading VOIs

import define_vois # Functions for defining VOIs
import register_masks # Functions for registering and processing masks
import transform_lib # Functions for transforming and loading data

# Constants for widget and display options
VOI_WIDGET = True
SHOW = True

# Define the imaging sites for analysis
SITES = [   "LUMC Discovery670",
            "MUMC Discovery870",
            "NKI Intevo",
            "NKI SymbiaT16",
            "Radboud Intevo",
            "Radboud SymbiaT16",
            "UMCU SymbiaT16",   
            ]

# Labels for acquisition / reconstruction protocols
LABELS = {
    "A": ["AcqA_DEW", "AcqA_TEW", "AcqA_HER"],
    "B": ["AcqB", "AcqB_HER"],
}

# Filenames for result and configuration files
RESULT_YAML = "results_NEMA.yml"
CONFIG_YAML = "config_data.yml"
NEMA_VOIS_PICKLE = "NEMA_VOIs.pickle"

if __name__ == "__main__":
    # Loop through each imaging site
    for SITE in SITES:
        print("Scanner analyzed:", SITE)
        # Load existing results if available, otherwise initialize an empty dictionary
        try:
            with open(RESULT_YAML, "r") as f:
                results = yaml.safe_load(f)
        except FileNotFoundError:
            results = {}

        # Load the configuration data
        with open(CONFIG_YAML, "r") as f:
            conf = yaml.safe_load(f)
        
        # Load saved VOIs if available
        try:
            saved_vois = pickle.load( open(NEMA_VOIS_PICKLE, "rb" ) )
        except FileNotFoundError:
            saved_vois = {}

        # Load CT template data
        CT_template = os.path.join(conf["Data folder"], conf["Site data"][SITE]["NEMA"]["CT_template"])
        dFix, sFix, pFix, oFix, headers = transform_lib.loadData(CT_template, return_headers=True)
        A_Fix, iA_Fix = transform_lib.genDICOMAffine3D(pFix, oFix, sFix)
        
        # Load or define VOIs
        if headers[0].SeriesInstanceUID in saved_vois and not VOI_WIDGET:
            sph_pos, lung_pos = saved_vois[headers[0].SeriesInstanceUID]
        else:
            VOI_widget = define_vois.Nema_VOI_Widget(dFix, sFix, headers[0].SeriesInstanceUID)
            sph_pos, lung_pos = VOI_widget.get_vois()
        
        # Determine acquisition list based on site
        if SITE == "LUMC Discovery670":
            acq_list = [1,2] # Limited dataset available for LUMC
        else:
            acq_list = [1,2,3]
        
        # Loop through each acquisition and protocol
        for i in acq_list:
            for acq in ["A","B"]: 
                try:
                    path_CT = os.path.join(conf["Data folder"], conf["Site data"][SITE]["NEMA"][str(i)+"_CT"+acq])
                except KeyError:
                    print("NOT DEFINED:", SITE, "NEMA", str(i)+"_CT"+acq)
                    continue
                
                # Load or create transformation
                trans = register_masks.get_Elastixtransform(CT_template, path_CT)
                dMov, sMov, pMov, oMov, headers = transform_lib.loadData( path_CT, return_headers=True  )
                A_Mov, iA_Mov = transform_lib.genDICOMAffine3D(pMov, oMov, sMov)
                FoR_CT = headers[0].FrameOfReferenceUID
                
                if SHOW: # Check if overlap is ok
                    register_masks.check_overlap(dFix, sFix, A_Fix, dMov, iA_Mov, trans, sph_pos)
                
                # Process data for each protocol label
                for label in LABELS[acq]:
                    try:
                        path_SPECT = os.path.join(conf["Data folder"], conf["Site data"][SITE]["NEMA"][str(i)+"_"+label])
                    except KeyError:
                        print("NOT DEFINED:", SITE, "NEMA", str(i)+"_"+label)
                        continue
                    
                    print(i, label)
                    
                    # Load SPECT data
                    dSpect, sSpect, pSpect, oSpect, headers = transform_lib.loadData( path_SPECT, return_headers=True )
                    A_Spect, iA_Spect = transform_lib.genDICOMAffine3D(pSpect, oSpect, sSpect)                
                    FoR_SPECT = headers.FrameOfReferenceUID
                    
                    # Warn if frame of reference UIDs do not match between CT and SPECT
                    if FoR_CT != FoR_SPECT:
                        print("WARNING: Frame of reference UID mismatch between CT and SPECT.")
                        print("CT path:", path_CT)
                        print("SPECT path:", path_SPECT)
                    
                    # Register SPECT to reference CT
                    dSpect_trans = register_masks.apply_transform(dFix, A_Fix, path_SPECT, trans)                
                    
                    # Apply post-filter to the SPECT data
                    for filter_size in [0, 5, 7.5, 10, 15]:
                        if filter_size == 0:
                            dSpect_trans_fltr = dSpect_trans[:]
                        else:                        
                            dSpect_trans_fltr = register_masks.filter(dSpect_trans, sFix, filter_size)
                        
                        # Apply the predefined mask to the registered SPECT
                        sphere_means, bgr_mean, bgr_std, lung_mean = register_masks.apply(dFix, sFix, dSpect_trans_fltr, sph_pos, lung_pos, show=SHOW)
                        
                        fill_ratio = conf["Site data"][SITE]["NEMA"]["Fill_Ratio"]
                        CRCs = [(sph_mean/bgr_mean-1)/(fill_ratio-1) for sph_mean in sphere_means]            
                        print(bgr_std/bgr_mean, CRCs, 1-lung_mean/bgr_mean)
                        
                        # Save results
                        res = results.setdefault(SITE, {})
                        res = res.setdefault(filter_size, {})
                        res = res.setdefault(label, {})
                        res[i] = {
                            "sphere_means": [float(x) for x in sphere_means],
                            "bgr_mean": float(bgr_mean),
                            "bgr_std": float(bgr_std),
                            "lung_mean": float(lung_mean),
                        }
                        
                        with open(RESULT_YAML, "w") as f:
                            yaml.dump(results, f, default_flow_style = None)

