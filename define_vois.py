import numpy as np
import scipy
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.widgets import Slider, RangeSlider, Button
import copy
import transform_lib

import pickle


NEMA_VOIS_PICKLE = "NEMA_VOIs.pickle"
SPHERES_MM = [10, 13, 17, 22, 28, 37]


class DraggablePoint:
    lock = None #only one can be animated at a time
    selected = None
    def __init__(self, point):
        self.point = point
        self.orig_size = (point.width, point.height)
        self.z_pos = None
        self.press = None
        self.background = None
        self.is_connected = False
        self.connect()    
    
    def toggle_zpos(self, z_pos):
        if self.z_pos is None and z_pos is not None:
            self.z_pos = z_pos
            self.point.set_facecolor('g')
        else:
            self.z_pos = None
            self.point.set_facecolor('r')
        self.point.width = self.orig_size[0]
        self.point.height = self.orig_size[1]
        
    def is_selected(self):
        return DraggablePoint.selected is self
    
    def connect(self):
        self.cidpress = self.point.figure.canvas.mpl_connect('button_press_event', self.on_press)
        self.cidrelease = self.point.figure.canvas.mpl_connect('button_release_event', self.on_release)
        self.cidmotion = self.point.figure.canvas.mpl_connect('motion_notify_event', self.on_motion)
        self.is_connected = True

    def on_press(self, event):
        if event.inaxes != self.point.axes: return
        if DraggablePoint.lock is not None: return
        contains, attrd = self.point.contains(event)
        if not contains: return
            
        self.press = (self.point.center), event.xdata, event.ydata
        DraggablePoint.lock = self        
        DraggablePoint.selected = self
        
        # draw everything but the selected rectangle and store the pixel buffer
        canvas = self.point.figure.canvas
        axes = self.point.axes
        self.point.set_animated(True)
        canvas.draw()
        self.background = canvas.copy_from_bbox(self.point.axes.bbox)

        # now redraw just the rectangle
        axes.draw_artist(self.point)

        # and blit just the redrawn area
        canvas.blit(axes.bbox)

    def on_motion(self, event):
        if DraggablePoint.lock is not self:
            return
        if event.inaxes != self.point.axes: return
                
        self.point.center, xpress, ypress = self.press
        dx = event.xdata - xpress
        dy = event.ydata - ypress
        self.point.center = (self.point.center[0]+dx, self.point.center[1]+dy)

        canvas = self.point.figure.canvas
        axes = self.point.axes
        # restore the background region
        canvas.restore_region(self.background)

        # redraw just the current rectangle
        axes.draw_artist(self.point)

        # blit just the redrawn area
        canvas.blit(axes.bbox)

    def on_release(self, event):
        'on release we reset the press data'
        if DraggablePoint.lock is not self:
            return

        self.press = None
        DraggablePoint.lock = None

        # turn off the rect animation property and reset the background
        self.point.set_animated(False)
        self.background = None

        # redraw the full figure
        self.point.figure.canvas.draw()

    def disconnect(self):
        'disconnect all the stored connection ids'
        self.point.figure.canvas.mpl_disconnect(self.cidpress)
        self.point.figure.canvas.mpl_disconnect(self.cidrelease)
        self.point.figure.canvas.mpl_disconnect(self.cidmotion)
        self.is_connected = False


class Nema_VOI_Widget(object):
    def __init__(self, data, pixsize, seriesUID):
        self.data = data
        self.pixsize = pixsize
        self.seriesUID = seriesUID
        
        try:
            self.SAVED_VOIS = pickle.load( open(NEMA_VOIS_PICKLE, "rb" ) )
        except FileNotFoundError:
            self.SAVED_VOIS = {}
        
        self.sphere_patches = []
        self.lung_patches = []
        
        initial_slice = self.data.shape[0]//2

        self.fig, self.ax = plt.subplots()
            
        self.axim = self.ax.imshow(self.data[initial_slice], cmap="gray", vmin=-20, vmax=80)
        
        YPOS = 60
        for i, diam in enumerate(SPHERES_MM):
            YPOS += 1.5 * diam
            center = (25/self.pixsize[0], YPOS/self.pixsize[1])
            circ = patches.Ellipse(center, diam/self.pixsize[0], diam/self.pixsize[1], fc='r', alpha=0.5)
            self.ax.add_patch(circ)        
            self.sphere_patches.append( DraggablePoint(circ) )
        
        for i in range(2):
            diam = 50
            YPOS = diam * (1-2*i)
            center = (self.data.shape[2]-50/self.pixsize[0], self.data.shape[1]/2 + YPOS/self.pixsize[1])
            circ = patches.Ellipse(center, diam/self.pixsize[0], diam/self.pixsize[1], fc='r', alpha=0.5)
            self.ax.add_patch(circ)
            self.lung_patches.append( DraggablePoint(circ) )
        
        self.lung_demo = patches.Ellipse((100, 100), 50/self.pixsize[0], 50/self.pixsize[1], fc='b', alpha=0.5, visible=False)
        self.ax.add_patch(self.lung_demo)
        
        ax_zoom =   plt.axes([0.82, 0.9, 0.16, 0.05])
        ax_slice =  plt.axes([0.82, 0.20, 0.05, 0.65])
        ax_window = plt.axes([0.92, 0.20, 0.05, 0.65])
        ax_button = plt.axes([0.82, 0.10, 0.16, 0.05])
        ax_save =   plt.axes([0.82, 0.05, 0.16, 0.05])
        
        autozoom_button = Button(ax_zoom, 'Autozoom', hovercolor='0.975')
        self.slice_slider = Slider(ax_slice, 'Slice', 0, len(self.data)-1, valstep=1, valinit=initial_slice, orientation='vertical')
        self.window_slider = RangeSlider(ax_window, "Window", self.data.min(), self.data.max(), valinit=(-50,150), orientation='vertical')
        zpos_button = Button(ax_button, 'Fix/unfix slice', hovercolor='0.975')
        save_button = Button(ax_save, 'Save', hovercolor='0.975')
                
        self.slice_slider.on_changed(self.update_slice)
        self.window_slider.on_changed(self.update_window)
        zpos_button.on_clicked(self.toggle_zpos)
        autozoom_button.on_clicked(self.autozoom)
        save_button.on_clicked(self.save)        
        self.fig.canvas.mpl_connect('scroll_event', self.scroll)
        
        self.fig.tight_layout()
        
        self.load_predefined()
        
        plt.show()
        
    
    def get_vois(self):
        sph_pos = []
        for dr in self.sphere_patches:
            sph_pos.append( (dr.z_pos, dr.point.center[1], dr.point.center[0]) )
        
        lung_pos = []
        for dr in self.lung_patches:
            lung_pos.append( (dr.z_pos, dr.point.center[1], dr.point.center[0]) )
        
        return sph_pos, lung_pos
    
        
    def load_predefined(self):
        # Set pre-defined mask positions
        predef_zpos = []
        sph_pos, lung_pos = self.SAVED_VOIS.get(self.seriesUID, [[None, None, None, None, None, None], [None, None]])
        for i, pos in enumerate(sph_pos):
            if pos is not None:
                if pos[0] is not None:
                    predef_zpos.append(pos[0])
                self.sphere_patches[i].toggle_zpos(pos[0])
                self.sphere_patches[i].point.center = (pos[2], pos[1])
        for i, pos in enumerate(lung_pos):
            if pos is not None:
                self.lung_patches[i].toggle_zpos(pos[0])
                self.lung_patches[i].point.center = (pos[2], pos[1])

        if len(predef_zpos):
            self.slice_slider.set_val(round(np.mean(predef_zpos)))
            
    
    def update_slice(self, event):
        cur_slice = int(self.slice_slider.val)
        
        self.axim.set_data(self.data[cur_slice])
        
        for jj, dr in enumerate(self.sphere_patches):
            if dr.z_pos is None:
                continue
            # Display the slice through the sphere depending on offset from the center
            diam_sq = SPHERES_MM[jj]**2 - (2*(cur_slice-dr.z_pos)*self.pixsize[2])**2
            diam = 0 if diam_sq < 0 else diam_sq**0.5
            
            self.sphere_patches[jj].point.width = diam/self.pixsize[0]
            self.sphere_patches[jj].point.height = diam/self.pixsize[1]
        
        # display lung mask
        for jj, dr in enumerate(self.lung_patches):
            if dr.z_pos is None or cur_slice == dr.z_pos:
                dr.point.set_visible(True)
            else:
                dr.point.set_visible(False)        
        
        # display the interpolated lung mask
        self.lung_demo.set_visible(False)
        if not any(dr.z_pos is None for dr in self.lung_patches):
            if self.lung_patches[0].z_pos == self.lung_patches[1].z_pos: return
            
            if self.lung_patches[0].z_pos < self.lung_patches[1].z_pos:
                l0 = self.lung_patches[0]
                l1 = self.lung_patches[1]
            else:
                l0 = self.lung_patches[1]
                l1 = self.lung_patches[0]
            
            if cur_slice > l0.z_pos and cur_slice < l1.z_pos:
                p0 = np.array([l0.point.center[0], l0.point.center[1]])
                p1 = np.array([l1.point.center[0], l1.point.center[1]])
                vec = p1 - p0
                lunglen = l1.z_pos - l0.z_pos
                center = p0 + (cur_slice - l0.z_pos) / lunglen * vec
                self.lung_demo.center = center
                self.lung_demo.set_visible(True)            
        
        self.fig.canvas.draw_idle()
    
    def scroll(self, event):
        cur_val = int(self.slice_slider.val)
        if event.button == 'up':
            if cur_val < len(self.data)-1:
                self.slice_slider.set_val(cur_val + 1)
        else:
            if cur_val > 0:
                self.slice_slider.set_val(cur_val - 1)
    
    def autozoom(self, event):
        thr = self.data[int(self.slice_slider.val)] > -50        
        com = scipy.ndimage.center_of_mass(thr)

        self.ax.set_xlim([com[1] - 150/self.pixsize[0], com[1] + 150/self.pixsize[0]])
        self.ax.set_ylim([com[0] + 120/self.pixsize[1], com[0] - 130/self.pixsize[1]])
        
        self.fig.canvas.draw_idle()
    
    def update_window(self, event):
        self.axim.set_clim(self.window_slider.val)
        self.fig.canvas.draw_idle()
            
    def toggle_zpos(self, event):
        for dr in self.sphere_patches + self.lung_patches:
            if dr.is_selected():
                dr.toggle_zpos( int(self.slice_slider.val) )
        self.fig.canvas.draw_idle()
    
    def save(self, event):  
        self.SAVED_VOIS[self.seriesUID] = self.get_vois()
        pickle.dump(self.SAVED_VOIS, open(NEMA_VOIS_PICKLE, "wb"))
    
    


if __name__ == "__main__":
    dicom_series = "Data2/1/B/CT LD Tissue 1_5 B31s"    
    dFix, sFix, pFix, oFix, headers = transform_lib.loadData( dicom_series, return_headers=True )    
    Nema_VOI_Widget(dFix, sFix, headers[0].SeriesInstanceUID)
