import numpy as np
from scipy.ndimage import affine_transform, gaussian_filter
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, RangeSlider, Button
import os
import pydicom
import tempfile
import pickle
import copy

import transform_lib


# Phantom specs
LUNG_INSERT_RADIUS = 15 # According to NEMA NU-2: Outside diameter of lung insert must be 50+/-2 mm.
SPHERE_CIRCLE_RADIUS = 57.2 # Distance of centers of spheres to center of phantom
SPHERES_MM = [10, 13, 17, 22, 28, 37]
SPHERES_ML = [4/3.*np.pi*(0.1*d/2.)**3 for d in SPHERES_MM]


def get_Elastixtransform(fixed_series, moving_series):
    elastix_transforms = pickle.load( open("elastix_transforms.pickle", "rb" ) )
     
    sid_fix = str(pydicom.read_file(os.path.join(fixed_series, os.listdir(fixed_series)[0])).SeriesInstanceUID)
    sid_mov = str(pydicom.read_file(os.path.join(moving_series, os.listdir(moving_series)[0])).SeriesInstanceUID)
    try:
        trans = elastix_transforms[sid_fix][sid_mov]
    except KeyError:        
        with tempfile.TemporaryDirectory() as tmpdir:
            trans = transform_lib.runElastix(fixed_series, moving_series, tmpdir)

        dct = elastix_transforms.setdefault(sid_fix, {})
        dct[sid_mov] = trans            
        elastix_transforms[sid_fix] = dct
        pickle.dump(elastix_transforms, open("elastix_transforms.pickle", "wb"))
        print(trans)
    return trans


def get_spheremask3D(shape, cz, cy, cx, rx, ry, rz):
    dz, dy, dx = shape
    z, y, x = np.ogrid[-cz:dz-cz, -cy:dy-cy, -cx:dx-cx]
    return ((x/rx)**2 + (y/ry)**2 + (z/rz)**2 <= 1)


def get_spheremask2D(shape, cy, cx, rx, ry):
    dy, dx = shape
    y, x = np.ogrid[-cy:dy-cy, -cx:dx-cx]
    return ((x/rx)**2 + (y/ry)**2 <= 1)    


def get_RC(value, background, fillratio):
    if fillratio != 0:
        return (value/background - 1) / (fillratio - 1)
    else:
        return 1 - value/background    


def get_CNR(r_mean,b_std,b_mean,n_vox): ####### EDIT #FIXME check definition
    contrast = r_mean - b_mean
    noise = b_std / np.sqrt(n_vox)
    return contrast / noise


def get_noise(b_std, b_mean): ######### EDIT
    noise = (b_std/b_mean)*100
    return noise


def mask_plot(image, roi=None, overlay_type='contour', **kwargs):
    plt.imshow(image, cmap='gray', interpolation='nearest', **kwargs)
    
    if roi is not None:
        if overlay_type == 'contour':
            plt.contour(roi, [0.5], colors='r')
        elif overlay_type == 'area':
            roi_cmap = plt.cm.jet
            roi_cmap.set_under(alpha=0)
            plt.imshow(roi, cmap=roi_cmap, alpha=0.5, vmin=0.5, interpolation='nearest')


def get_linethroughlung(dSpect_trans, center):
    zw = 3
    theslice = np.s_[int(center[0])-zw : int(center[0])+zw, int(center[1]), :]
    data = dSpect_trans[theslice].mean(axis=(0))
    data /= data.sum()
    return data


def get_linebelowlung(dSpect_trans, center):
    zw = 3
    theslice = np.s_[int(center[0])-zw : int(center[0])+zw, int(center[1]), :]
    data = dSpect_trans[theslice].mean(axis=(0))
    data /= data.sum()
    return data


def check_overlap(dFix, sFix, A_Fix, dMov, iA_Mov, trans, sph_pos):
    A_TR = iA_Mov @ trans @ A_Fix
    dMov_trans = affine_transform(dMov, A_TR, output_shape=dFix.shape, order=0)

    fig, axs = plt.subplots(1,3, figsize=(16,12))        
    for i in range(3):
        slicing = (slice(None),) * i + (int(sph_pos[-1][i]),) 
        axs[i].imshow(dFix[slicing], cmap="Reds", vmin=-100, vmax=100)
        axs[i].imshow(dMov_trans[slicing], cmap="Greens", alpha=0.5, vmin=-100, vmax=100)
        if i != 0:
            axs[i].set_aspect(sFix[2]/sFix[1])
            axs[i].set_aspect(sFix[2]/sFix[1])
    
    ax_alpha = plt.axes([0.95, 0.20, 0.04, 0.65])
    alpha_slider = Slider(ax_alpha, "Alpha", 0, 1, valinit=0.5, orientation='vertical')
    def update_alpha(event):
        for i in range(3):
            axs[i].get_images()[1].set_alpha(alpha_slider.val)
        fig.canvas.draw_idle()
    alpha_slider.on_changed(update_alpha)
    update_alpha(None)
    
    plt.show()    


def get_backgroundmask(shape, pixsize, sph_pos, lung_pos):
    rbig = SPHERE_CIRCLE_RADIUS + SPHERES_MM[-1]/2
    rsmall = SPHERE_CIRCLE_RADIUS - SPHERES_MM[-1]/2

    ry_big, rx_big = rbig/pixsize[:2]
    ry_small, rx_small = rsmall/pixsize[:2]
    
    sphere_idx = np.mean([p[0] for p in sph_pos])
    
    lung_pos.sort()
    p0 = np.array(lung_pos[0])
    p1 = np.array(lung_pos[1])
    vec = p1 - p0
    lunglen = p1[0] - p0[0]
    
    bgr_pos = p0 + (1 - (sphere_idx - p0[0])/lunglen) * vec

    cz, cy, cx = bgr_pos
    y,x = np.ogrid[-cy:shape[1]-cy, -cx:shape[2]-cx]
    mask = ((x/rx_big)**2 + (y/ry_big)**2 < 1) & ((x/rx_small)**2 + (y/ry_small)**2 > 1)
    
    width = int(np.ceil( 25 / pixsize[-1] )) 
    
    bgr_mask = np.zeros(shape, dtype=bool)
    bgr_mask[int(cz)-width:int(cz)+width] = mask
    return bgr_pos, bgr_mask


def get_lungmask(shape, pixsize, lung_pos):
    lung_pos.sort()
    p0 = np.array(lung_pos[0])
    p1 = np.array(lung_pos[1])
    vec = p1 - p0
    lunglen = p1[0] - p0[0]
    
    lung_mask = np.zeros(shape, dtype=bool)
    for idx in range(int(p0[0]), int(p1[0]+1)):
        cz, cy, cx = p0 + (idx - p0[0])/lunglen * vec
        lung_radius = LUNG_INSERT_RADIUS/pixsize[1]
        y,x = np.ogrid[-cy:shape[1]-cy, -cx:shape[2]-cx]
        mask = ((x/lung_radius)**2 + (y/lung_radius)**2 <= 1)    
        lung_mask[idx] = mask
    return lung_mask


def apply_transform(dFix, A_Fix, spect_series, trans):
    # SPECT image
    dSpect, sSpect, pSpect, oSpect = transform_lib.loadData( spect_series )
    A_Spect, iA_Spect = transform_lib.genDICOMAffine3D(pSpect, oSpect, sSpect)    

    # Register SPECT to reference CT:
    A_TR = iA_Spect @ trans @ A_Fix
    return affine_transform(dSpect, A_TR, output_shape=dFix.shape, order=1)

def apply_transform_CT(dFix, A_Fix, ct_series, trans):
    # CT image
    dCt, sCt, pCt, oCt = transform_lib.loadData( ct_series )
    A_Ct, iA_Ct = transform_lib.genDICOMAffine3D(pCt, oCt, sCt)    

    # Register CT to SPECT:
    A_TR = iA_Ct @ trans @ A_Fix
    return affine_transform(dCt, A_TR, output_shape=dFix.shape, order=1, mode='constant', cval=-1000)


def filter(dSpect_trans, sFix, width_fwhm):
    width_sigma = width_fwhm / 2.35482
    # print("Width sigma: " + str(width_sigma))
    return gaussian_filter(dSpect_trans, width_sigma/sFix[::-1], mode="constant")


def apply(dFix, sFix, dSpect_trans, sph_pos, lung_pos, show=True):    
    all_masks = np.zeros(dFix.shape, dtype=bool)
    
    bgr_pos, bgr_mask = get_backgroundmask(dFix.shape, sFix, sph_pos, lung_pos)
    bgr_mean = dSpect_trans[bgr_mask].mean()
    bgr_std  = dSpect_trans[bgr_mask].std()
    all_masks[bgr_mask] = True
    
    lung_mask = get_lungmask(dFix.shape, sFix, lung_pos)
    lung_mean = dSpect_trans[lung_mask].mean()
    all_masks[lung_mask] = True
    
    sphere_means = []
    for i in range(6):
        radius = SPHERES_MM[i]/2.
        rx, ry, rz = radius / sFix
        cz, cy, cx = sph_pos[i]
        
        mask = get_spheremask3D(dFix.shape, cz, cy, cx, rx, ry, rz)
        all_masks[mask] = True
        
        sphere_means.append(dSpect_trans[mask].mean())
    
    
    if show:
        fig, axs = plt.subplots(2,3, figsize=(16,12), gridspec_kw={'height_ratios': [8,1]})#, dpi=100)
        C = []      
        cmap = copy.copy(plt.cm.hot)
        cmap.set_under(alpha=0)
        for i in range(3):
            slicing = (slice(None),) * i + (int(sph_pos[-1][i]),)
            axs[0,i].imshow(dFix[slicing], cmap="gray", vmin=-50, vmax=150)
            axs[0,i].imshow(dSpect_trans[slicing], cmap=cmap, alpha=0.5)
            C.append( axs[0,i].contour(all_masks[slicing], [0.5], linewidths=2, colors='b', linestyles=[(0, (3, 3))]) )                        
            if i != 0:
                axs[0,i].set_aspect(sFix[2]/sFix[1])
        
        # Window-level of CT
        ax_CTwindow = plt.axes([0.9, 0.20, 0.025, 0.65])
        CTwindow_slider = RangeSlider(ax_CTwindow, "CT", dFix.min(), dFix.max(), valinit=(-50,150), orientation='vertical')
        def update_CTwindow(event):
            for i in range(3):
                axs[0,i].get_images()[0].set_clim(CTwindow_slider.val)
            fig.canvas.draw_idle()        
        CTwindow_slider.on_changed(update_CTwindow)
        
        # Window-level of SPECT
        mn, mx = dSpect_trans.min(), dSpect_trans.max()
        ax_SPECTwindow = plt.axes([0.93, 0.20, 0.025, 0.65])
        SPECTwindow_slider = RangeSlider(ax_SPECTwindow, "SPECT", mn, mx, valinit=(mn, mx), orientation='vertical')
        def update_SPECTwindow(event):
            for i in range(3):
                axs[0,i].get_images()[1].set_clim(SPECTwindow_slider.val)
            fig.canvas.draw_idle()        
        SPECTwindow_slider.on_changed(update_SPECTwindow)
        
        # Alpha channel of SPECT
        ax_alpha = plt.axes([0.96, 0.20, 0.025, 0.65])
        alpha_slider = Slider(ax_alpha, "Alpha", 0, 1, valinit=0.5, orientation='vertical')
        def update_alpha(event):
            for i in range(3):
                axs[0,i].get_images()[1].set_alpha(alpha_slider.val)
            fig.canvas.draw_idle()
        alpha_slider.on_changed(update_alpha)
        update_alpha(None)
        
        # Scroll through slices        
        def update_slice(column):
            slicing = (slice(None),) * column + (int(sliders[column].val),)            

            axs[0,column].get_images()[0].set_data(dFix[slicing])
            axs[0,column].get_images()[1].set_data(dSpect_trans[slicing])
            
            for coll in C[column].collections: #Ugly hack to update contour plots: remove and create new
                coll.remove()
            C[column] = axs[0,column].contour(all_masks[slicing], [0.5], linewidths=2, colors='b', linestyles=[(0, (3, 3))])
            fig.canvas.draw_idle()
         
        sliders = []
        for i in range(3):            
            sliders.append( Slider(axs[1,i], None, 0, dFix.shape[i]-1, valstep=1, valinit=int(sph_pos[-1][i]), orientation='horizontal') )
            sliders[i].on_changed( lambda x, i=i: update_slice(i) )
        
        plt.show()
    
    return sphere_means, bgr_mean, bgr_std, lung_mean

def apply_dilation(dFix, sFix, dSpect_trans, sph_pos, lung_pos, sph_dilation, show=True):    
    all_masks = np.zeros(dFix.shape, dtype=bool)
    
    bgr_pos, bgr_mask = get_backgroundmask(dFix.shape, sFix, sph_pos, lung_pos)
    bgr_mean = dSpect_trans[bgr_mask].mean()
    bgr_std  = dSpect_trans[bgr_mask].std()
    all_masks[bgr_mask] = True
    
    lung_mask = get_lungmask(dFix.shape, sFix, lung_pos)
    lung_mean = dSpect_trans[lung_mask].mean()
    all_masks[lung_mask] = True
    
    sphere_dilation_means = []
    for i in range(6):
        radius = SPHERES_MM[i]/2
        radius_dilated = SPHERES_MM[i]/2.+sph_dilation
        rx, ry, rz = radius / sFix
        cz, cy, cx = sph_pos[i]
        
        rx_dilated, ry_dilated, rz_dilated = radius_dilated / sFix
        
        
        mask = get_spheremask3D(dFix.shape, cz, cy, cx, rx, ry, rz)
        # all_masks[mask] = True
        
        mask_dilated = get_spheremask3D(dFix.shape, cz, cy, cx, rx_dilated, ry_dilated, rz_dilated)
        all_masks[mask_dilated] = True
        
        # mlPerVoxel = np.prod(sFix/10)
        # num_voxels_dilated = ((4/3.*np.pi*(0.1*radius_dilated)**3) - (4/3.*np.pi*(0.1*radius)**3))/mlPerVoxel
        num_voxels_dilated = mask_dilated.sum() -mask.sum() 
        # print("num_voxels ", num_voxels_dilated) 
        # dilation_corrected_mean = (dSpect_trans[mask_dilated].sum()-bgr_mean*num_voxels_dilated)/mask.sum()
        
        nrm_cts = np.sum(dSpect_trans[mask], dtype=np.uint64)
        dil_cts = np.sum(dSpect_trans[mask_dilated], dtype=np.uint64)
        
        dilation_corrected_mean = (dil_cts-bgr_mean*num_voxels_dilated)/mask.sum()

        # print(dil_cts)
        # print(nrm_cts)
        # print(bgr_mean*num_voxels_dilated)
        # sphere_means.append(dSpect_trans[mask].mean())
        sphere_dilation_means.append(dilation_corrected_mean)

    
    if show:
        fig, axs = plt.subplots(2,3, figsize=(16,12), gridspec_kw={'height_ratios': [8,1]})#, dpi=100)
        C = []      
        cmap = copy.copy(plt.cm.hot)
        cmap.set_under(alpha=0)
        for i in range(3):
            slicing = (slice(None),) * i + (int(sph_pos[-1][i]),)
            axs[0,i].imshow(dFix[slicing], cmap="gray", vmin=-50, vmax=150)
            axs[0,i].imshow(dSpect_trans[slicing], cmap=cmap, alpha=0.5)
            C.append( axs[0,i].contour(all_masks[slicing], [0.5], linewidths=2, colors='b', linestyles=[(0, (3, 3))]) )                        
            if i != 0:
                axs[0,i].set_aspect(sFix[2]/sFix[1])
        
        # Window-level of CT
        ax_CTwindow = plt.axes([0.9, 0.20, 0.025, 0.65])
        CTwindow_slider = RangeSlider(ax_CTwindow, "CT", dFix.min(), dFix.max(), valinit=(-50,150), orientation='vertical')
        def update_CTwindow(event):
            for i in range(3):
                axs[0,i].get_images()[0].set_clim(CTwindow_slider.val)
            fig.canvas.draw_idle()        
        CTwindow_slider.on_changed(update_CTwindow)
        
        # Window-level of SPECT
        mn, mx = dSpect_trans.min(), dSpect_trans.max()
        ax_SPECTwindow = plt.axes([0.93, 0.20, 0.025, 0.65])
        SPECTwindow_slider = RangeSlider(ax_SPECTwindow, "SPECT", mn, mx, valinit=(mn, mx), orientation='vertical')
        def update_SPECTwindow(event):
            for i in range(3):
                axs[0,i].get_images()[1].set_clim(SPECTwindow_slider.val)
            fig.canvas.draw_idle()        
        SPECTwindow_slider.on_changed(update_SPECTwindow)
        
        # Alpha channel of SPECT
        ax_alpha = plt.axes([0.96, 0.20, 0.025, 0.65])
        alpha_slider = Slider(ax_alpha, "Alpha", 0, 1, valinit=0.5, orientation='vertical')
        def update_alpha(event):
            for i in range(3):
                axs[0,i].get_images()[1].set_alpha(alpha_slider.val)
            fig.canvas.draw_idle()
        alpha_slider.on_changed(update_alpha)
        update_alpha(None)
        
        # Scroll through slices        
        def update_slice(column):
            slicing = (slice(None),) * column + (int(sliders[column].val),)            

            axs[0,column].get_images()[0].set_data(dFix[slicing])
            axs[0,column].get_images()[1].set_data(dSpect_trans[slicing])
            
            for coll in C[column].collections: #Ugly hack to update contour plots: remove and create new
                coll.remove()
            C[column] = axs[0,column].contour(all_masks[slicing], [0.5], linewidths=2, colors='b', linestyles=[(0, (3, 3))])
            fig.canvas.draw_idle()
         
        sliders = []
        for i in range(3):            
            sliders.append( Slider(axs[1,i], None, 0, dFix.shape[i]-1, valstep=1, valinit=int(sph_pos[-1][i]), orientation='horizontal') )
            sliders[i].on_changed( lambda x, i=i: update_slice(i) )
        
        plt.show()
    
    return sphere_dilation_means, bgr_mean, bgr_std, lung_mean
    

