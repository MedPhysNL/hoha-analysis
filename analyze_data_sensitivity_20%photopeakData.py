"""
This script calculates the system sensitivity as presented in Table S.1. The calculation 
is based on the projection data (raw SPECT data) and the activity present in the 
phantom at scan time (filled in below in `dataset`).

Key features:
- Sensitivity is calculated for each energy window by processing pixel data from the 
  DICOM files and normalizing it by acquisition parameters and activity levels.
- Results are visualized as scatter plots, with sensitivity plotted against activity for
  each energy window and imaging system.
- Supports multiple imaging systems, defined in the `dataset` dictionary, which contains
  file paths, activity values, and system names.

Dependencies:
- matplotlib: For generating plots.
- pydicom: For reading and processing DICOM files.

Output:
- A figure visualizing sensitivity for all systems across the defined energy windows.
- Sensitivity data is printed in the console for further inspection.
"""

# Import necessary libraries
import os  # For file and path operations
import matplotlib.pyplot as plt  # For creating and displaying plots
import pydicom                   # For handling and reading DICOM files

# Define a list of colors for use in plotting different datasets
colors = ['tab:green', 'tab:blue', 'tab:orange', 'tab:purple', 'tab:cyan', 
          'tab:olive', 'tab:gray', 'tab:brown', 'tab:pink']

# Define labels for the energy windows being analyzed
energyLabels = ["Low scatter", "Photopeak", "High scatter"]

# Define a dataset containing file paths, activity values, and names for various datasets
dataset = {
    "MUMC": {  # Maastricht University Medical Center
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/MUMC/GE Discovery 870/Cylinder/1/B/Cylinder acqB/TomoHo166B_Ho166001_DS.dcm",
        "act": 286,  # Activity value in MBq
        "name": "GE 870",  # Scanner name
        },
    "NKI_Intevo": {  # Netherlands Cancer Institute, Intevo Scanner
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/NKI/Intevo/Cylinder/1/B/Cylinder acqB/2.25.46778469555775593798199530973108187861",
        "act": 263,
        "name": "Siemens Intevo 1",
        },
    "Radboud_Intevo": {  # Radboud University, Intevo Scanner
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/Radboud/Intevo/Cylinder/1/B/Cylinder acqB/HO_HARMONIZATION_Radboud(CylB)",
        "act": 281,
        "name": "Siemens Intevo 2",
        },
    "NKI_Symbia": {  # Netherlands Cancer Institute, Symbia Scanner
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/NKI/SymbiaT16/Cylinder/1/B/Cylinder acqB/2.25.47527646569051083249801024210387428355",
        "act": 273,
        "name": "Siemens Symbia 1",
        },
    "Radboud_Symbia": {  # Radboud University, Symbia Scanner
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/Radboud/SymbiaT16/Cylinder/1/B/Cylinder acqB/HO_HARMONIZATION_Radboud(CylB)",
        "act": 310,
        "name": "Siemens Symbia 2",
        },
    "UMCU": {  # University Medical Center Utrecht
        "fn": "C:/Users/Lovisa/surfdrive/Shared/Holmium_protocol_harmonisatie/WorkInProgress/Data_clean/UMCU/SymbiaT16/Cylinder/1/B/Cylinder acqB/HO_HARMONIZATION_UMCU.NM._.1000.0001.2022.01.30.07.56.51.572640.11899556.IMA",
        "act": 327,
        "name": "Siemens Symbia 3",
        },
}

# Function to calculate sensitivity values from a DICOM file
def get_sensitivity(fn, act, name):
    ds = pydicom.read_file(fn, force=True)  # Read the DICOM file
    data = ds.pixel_array  # Extract the pixel data array

    # Calculate the acquisition time in seconds
    count_time_s = ds.RotationInformationSequence[0].ActualFrameDuration / 1000
    # Calculate the number of projections per energy window
    numProjections = int(ds.NumberOfFrames / ds.NumberOfEnergyWindows)
  
    # Loop through the energy windows defined in the DICOM metadata
    ew = ds.EnergyWindowInformationSequence
    for i, ewin in enumerate(ew):
        rs = ewin.EnergyWindowRangeSequence
        for r in rs:  # Loop through each energy range in the window
            low = r.EnergyWindowLowerLimit
            high = r.EnergyWindowUpperLimit
            # Check which energy window the data belongs to and assign accordingly
            if 68 > low and 68 < high:
                sc_low = data[i*numProjections:(i+1)*numProjections]  # Low scatter
            if 81 > low and 81 < high:
                pp = data[i*numProjections:(i+1)*numProjections]  # Photopeak
            if 90 > low and 90 < high:
                sc_high = data[i*numProjections:(i+1)*numProjections]  # High scatter

    # Calculate sensitivity values for each energy window
    sens_low_whole = sc_low.sum() / (numProjections * count_time_s) / act
    sens_pp_whole = pp.sum() / (numProjections * count_time_s) / act
    sens_high_whole = sc_high.sum() / (numProjections * count_time_s) / act

    # Round the sensitivity values for easier readability
    data_wholeProjection_round = [name, round(sens_low_whole, 2), 
                                   round(sens_pp_whole, 2), 
                                   round(sens_high_whole, 2)]
    
    # Print the sensitivity values for debugging
    print("Whole projection")
    print(round(sens_low_whole, 2))
    print(round(sens_pp_whole, 2))
    print(round(sens_high_whole, 2))
        
    # Return the rounded sensitivity values
    return data_wholeProjection_round

# Main script entry point
if __name__ == "__main__":    
    table_fullProjections = []  # Initialize a table to store results
    # Create subplots for each energy window
    fig_wholeProjection, ax_wholeProjection = plt.subplots(1, 3, figsize=(10, 3.5), sharey=True)

    icenter = 0  # Initialize index for color selection
    for center, center_data in dataset.items():  # Loop through each dataset
        print(center)  # Print the name of the center
        # Compute sensitivity values
        data_wholeProjection = get_sensitivity(center_data["fn"], center_data["act"], center_data["name"])
        table_fullProjections.append(data_wholeProjection)  # Append results
        print()

        # Plot the sensitivity values for each energy window
        for energyWindow in range(len(data_wholeProjection) - 1):
            ax_wholeProjection[energyWindow].plot(center_data["act"], 
                                                  data_wholeProjection[energyWindow + 1], 
                                                  'o', label=center_data["name"], 
                                                  color=colors[icenter])
            
            # Set labels and gridlines for each subplot
            if energyWindow == 0:
                ax_wholeProjection[energyWindow].set_ylabel("Sensitivity [cps/MBq]")
            if energyWindow == 2:
                ax_wholeProjection[energyWindow].legend(loc='upper right', fancybox=True, bbox_to_anchor=(2, 0.95))
                
            ax_wholeProjection[energyWindow].set_xlabel("Activity [MBq]")
            ax_wholeProjection[energyWindow].set_title(energyLabels[energyWindow])
            ax_wholeProjection[energyWindow].set_ylim(bottom=0, top=11)
            ax_wholeProjection[energyWindow].grid()
        
        icenter += 1  # Increment color index for the next dataset

    # Set the overall title for the figure
    fig_wholeProjection.suptitle("Whole projection")
    plt.draw()  # Prepare the plot for display
    plt.show()  # Display the plot

    # Save the figure to a file (commented out by default)
    fn_wholeProjection = "Sensitivity_wholeProjection_acqB.png"
    # Uncomment to save: fig_wholeProjection.savefig(os.path.join("Figures", fn_wholeProjection), dpi=500, orientation='landscape', format='png', bbox_inches='tight')
